<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- bootstrap css -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css">
    <!-- custom css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend.css') }}">

    <!-- needed javascripts -->
	<script src="{{ asset('assets/global/plugins/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/global/plugins/jquery-1.9.1.min.js') }}"></script>
	<script src="{{ asset('assets/global/scripts/app.js') }}"></script>
	<script src="{{ asset('assets/global/scripts/app.min.js') }}"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}"></script>
	<!-- custom javascripts -->
	<script type="text/javascript">
		function myFunction() {
		    document.getElementById("myDropdown").classList.toggle("show");
		}

		$(document).ready(function(){
			/*var barcode = document.getElementById("barcode");
			barcode.addEventListener("keyup", function(event) {
				event.preventDefault();
				if (event.keyCode == 13) {
		            $("#forModal").click(function(){
			            $("#myModal").modal();
			            $(".forquantity").css('display', 'table');
			            $(".forf1").css('display', 'none');
			        	$(".forf2").css('display', 'none');
			        	$(".forf3").css('display', 'none');
			        	$(".forf4").css('display', 'none');
			            $(".input-group").css('display', 'table');
			            $("#barcodeqty").val(jQuery("#barcode").val());
			        	$(".f2prod").css('display', 'none');
			        	$(".f1prod").css('display', 'none');
			        	$(".f3prod").css('display', 'none');
			        	$(".f4prod").css('display', 'none');
			        });
		        }
			});*/
			$("#barcode").focus();
			$("#forModal").click(function(){
	            $("#myModal").modal();
	            $(".forquantity").css('display', 'table');
	            $(".input-group").css('display', 'table');
	            $("#barcodeqty").val(jQuery("#barcode").val());
	            $(".forf1").css('display', 'none');
	        	$(".forf2").css('display', 'none');
	        	$(".forf3").css('display', 'none');
	        	$(".forf4").css('display', 'none');
	        	$(".f2prod").css('display', 'none');
	        	$(".f1prod").css('display', 'none');
	        	$(".f3prod").css('display', 'none');
	        	$(".f4prod").css('display', 'none');
	        });
	        $("#f1").click(function() {
	        	$("#myModal").modal();
	        	$(".forf1").css('display', 'block');
	        	$(".forf2").css('display', 'none');
	        	$(".forf3").css('display', 'none');
	        	$(".forf4").css('display', 'none');
	        	$(".forquantity").css('display', 'none');
	        	$(".f1prod").css('display', 'block');
	        	$(".f2prod").css('display', 'none');
	        	$(".f3prod").css('display', 'none');
	        	$(".f4prod").css('display', 'none');
	        	$(".input-group").css('display', 'none');
	        });
	        $("#f2").click(function() {
	        	$("#myModal").modal();
	        	$(".forf2").css('display', 'block');
	        	$(".forf1").css('display', 'none');
	        	$(".forf3").css('display', 'none');
	        	$(".forf4").css('display', 'none');
	        	$(".forquantity").css('display', 'none');
	        	$(".f2prod").css('display', 'block');
	        	$(".f1prod").css('display', 'none');
	        	$(".f3prod").css('display', 'none');
	        	$(".f4prod").css('display', 'none');
	        	$(".input-group").css('display', 'none');
	        });
	        $("#f3").click(function() {
	        	$("#myModal").modal();
	        	$(".forf3").css('display', 'block');
	        	$(".forf2").css('display', 'none');
	        	$(".forf1").css('display', 'none');
	        	$(".forf4").css('display', 'none');
	        	$(".forquantity").css('display', 'none');
	        	$(".f3prod").css('display', 'block');
	        	$(".f1prod").css('display', 'none');
	        	$(".f2prod").css('display', 'none');
	        	$(".f4prod").css('display', 'none');
	        	$(".input-group").css('display', 'none');
	        });
	        $("#f4").click(function() {
	        	$("#myModal").modal();
	        	$(".forf4").css('display', 'block');
	        	$(".forf2").css('display', 'none');
	        	$(".forf3").css('display', 'none');
	        	$(".forf1").css('display', 'none');
	        	$(".forquantity").css('display', 'none');
	        	$(".f4prod").css('display', 'block');
	        	$(".f1prod").css('display', 'none');
	        	$(".f2prod").css('display', 'none');
	        	$(".f3prod").css('display', 'none');
	        	$(".input-group").css('display', 'none');
	        });

	        /*var input = document.getElementById("myInput");*/
			addEventListener("keyup", function(event) {
			    event.preventDefault();
			    if (event.keyCode === 13) {
			        var datanum = jQuery("#forModal").attr("data-number");
			        if (datanum == 0) {
			        	document.getElementById("forModal").click();
			        	jQuery("#forModal").attr("data-number", "1");
			        	jQuery("#qtyinput").focus();
			        } else {
			        	document.getElementById("payamount").click();
			        	jQuery("#forModal").attr("data-number", "0");
			        	alert("clicked");
			        }
			    } else if (event.keyCode === 112) {
			    	document.getElementById("f1").click();
			    } else if (event.keyCode === 113) {
			    	document.getElementById("f2").click();
			    } else if (event.keyCode === 114) {
			    	document.getElementById("f3").click();
			    } else if (event.keyCode === 115) {
			    	document.getElementById("f4").click();
			    } else if (event.keyCode === 27) {
			    	document.getElementById("close").click();

			    	var datanum2 = jQuery("#forModal").attr("data-number");
			    	if (datanum2 == 1) {
			    	jQuery("#barcode").focus();
			    	}
			    	jQuery("#forModal").attr("data-number", "0");
			    }
			});

			$("#close").click(function() {
				var datanum2 = jQuery("#forModal").attr("data-number");
		    	if (datanum2 == 1) {
		    	jQuery("#barcode").focus();
		    	}
		    	jQuery("#forModal").attr("data-number", "0");
	        });
		});
	</script>
</head>