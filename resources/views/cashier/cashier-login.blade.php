@extends('layouts.custom-layout')

@section('content')
<div class="container loginc">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- <div class="panel panel-default">
                <div class="panel-heading">Login</div> -->

            <div class="panel-body">
                <h3 class="admintit">Sign In Here Cashier</h3>
                <form class="form-horizontal" method="POST" action="{{ route('cashier.login.submit') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                        <div class="col-md-8 col-md-offset-2">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

                        <div class="col-md-8 col-md-offset-2">
                            <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="checkbox">
                                <label>
                                    <input id="rem" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="remem">Remember Me</span>
                                </label>
                            </div>
                            <div class="forget">
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    <span class="for"> Forgot Your Password? </span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-default" id="ctbtn">
                                Login
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- </div>
        </div> -->
    </div>
</div>
@endsection

<style type="text/css">
    body {
        background-image: url("{{url('/')}}/assets/apps/img/green_grass_field.jpg");
        background-repeat: no-repeat; 
    }
    .loginc {
        padding: 80px;
    }
    .admintit {
        color: #2e6b42;
        text-align: center;
        padding-bottom: 20px;
    }
    #email {
        border-radius: 40px;
        height: 50px;
        font-size: 17px;
        padding: 16px 10px 15px 35px;
        font-family: 'Quicksand', sans-serif;
        color: #2e6b42;
    }
    #password {
        border-radius: 40px;
        height: 50px;
        font-size: 17px;
        padding: 16px 10px 15px 35px;
        font-family: 'Quicksand', sans-serif;
        color: #2e6b42;
    }
    #rem {
        width: 20px !important;
        height: 20px !important;
        position: relative;
        left: 15px;
        bottom: 4px;
    }
    .remem {
        position: relative;
        left: 18px;
        bottom: 8px;
        color: #2e6b42;
    }
    .checkbox {
        float: left;
    }
    .forget {
        float: right;
    }
    .for {
        color: #2e6b42;
    }
    .for:hover {
        text-decoration: underline;
        text-decoration-color: #2e6b42; 
        color: #2e6b42;
    }
    #ctbtn {
        border-radius: 40px;
        background-color: transparent;
        height: 45px;
        width: 200px;
        color: #2e6b42;
    }
    .tit {
        padding-top: 14px;
        padding-bottom: 14px;
        padding-right: 5px;
        color: #2e6b42;
        font-weight: 400;
    }
    .ctre {
        position: relative;
        display: inline-block;
        top: 14px;
    }
    .ctre > a {
        color: #ffffff;
    }
    .ctre > a:hover {
        color: #ffffff;
        text-decoration: underline;
        text-decoration-color: #a7b7ba; 
    }
</style>