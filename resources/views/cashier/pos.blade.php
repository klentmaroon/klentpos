@include('cashier.posheadtags')

<body class="page-wrapper">
	<header class="header">
		<div class="row">
			<div class="topnav">
			  <img src="https://arizonachristian.edu/wp-content/uploads/2017/06/logo-placeholder.png" class="navlogo">
			  <a class="active" href="#"> Dashboard </a>
		</div>
	</header>

	<div class="blankspace"></div>

	<div class="content">
		<div class="row">
			<div class="col-md-6">
				<!-- <label for="barcode" class="barlabel"> BARCODE: </label>
				<input type="text" name="barcode" id="barcode" placeholder="#" value="">
				<input type="button" name="barcodesub" id="forModal" data-number="0" hidden=""> -->
				<!-- <a href="#" id="forModal" class="btn btn-default qty">
					<i class="fa fa-plus-circle"></i>
                    Add Quantity
				</a> -->
			</div>

			<div class="col-md-6">
				<li class="dropdown dropdown-user" style="float: right;padding: 7px;">
                    <a href="#" class="btn btn-default downar dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    	<span class="username"><b> {{ Auth::user()->name }} </b></span>
                    	<i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right subdrop">
                        <li>
                            <a href="{{ route('cashier.logout') }}" class="dropopt" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            	<i class="fa fa-user"></i> Logout 
                            </a>
                            <form id="logout-form" action="{{ route('cashier.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                        </li>
                    </ul>
                </li>
			</div>
		</div>
	</div>

	<div class="row" style="margin-bottom: 20px;">
		<div class="col-md-12">
			<div class="col-md-7">
				<div class="displayarea">
					<div class="displayareaheader">
						<!-- <a href="#" id="forModal" class="btn btn-default qty">
							<i class="fa fa-plus-circle"></i>
                            Add Quantity
						</a> -->
						<a href="#" class="btn btn-default" style="float: right;margin-top: 13px;margin-right: 20px;border-radius: 25px !important;"> Cash In/Out </a>
					</div>
					<div class="displayareacontentleft">
						<table class="table table-striped table-bordered table-advance" id="customtables">
							<thead>
								<tr>
									<td style="text-align: center;"> Quantity </td>
									<td style="text-align: center;"> Product </td>
									<td style="text-align: center;"> Price </td>
									<td style="text-align: center;"> Total Price </td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align: center;"> 5 </td>
									<td> Cystic Acne Hair Repair Treatment </td>
									<td style="text-align: center;"> $100 </td>
									<td style="text-align: center;"> $500 </td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="displayareafooter">
						<label for="barcode" class="barlabel"> BARCODE: </label>
						<input type="text" name="barcode" id="barcode" placeholder="#" value="">
						<input type="button" name="barcodesub" id="forModal" data-number="0" hidden="">
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="displayarea">
					<div class="displayareaheader">
						<p class="total">  </p>
					</div>
					<div class="displayareacontentright">
						<div class="col-md-7 purdetails">
							<div class="texts"> Sub Total: </div>
							<div class="texts"> Tax Rate: </div>
							<div class="texts" style="font-weight: bold;"> Total Amount: </div>
							<div class="texts" style="font-weight: bold;"> Amount Received: </div>
							<div class="texts" style="font-weight: bold;"> Change: </div>
						</div>
						<div class="col-md-5 purdetails">
							<input type="number" name="subtot" step="any" min="0" placeholder="$1,000,000000000000" class="amountrec" value="" readonly="">
							<input type="number" name="tax" step="any" min="0" placeholder="$0.00" class="amountrec" value="" readonly="">
							<input type="number" name="totalamount" step="any" min="0" placeholder="$0.00" class="amountrec" value="" readonly="" style="font-weight: bold;">
							<input type="number" name="amountreceived" id="amountinput" step="any" min="0" placeholder="Cash" class="amountrec" value="" style="font-weight: bold;">
							<input type="number" name="change" step="any" min="0" placeholder="$0.00" class="amountrec" value="" readonly="" style="font-weight: bold;">
						</div>
					</div>
					<div class="displayareafooter">
						<div class="paybtn">
							<a href="" class="btn btn-default dark">
								<i class="fa fa-credit-card" style="color: #fff;"></i>
								<span class="pay"> Suki Card </span>
							</a>
							<a href="#" class="btn btn-default custom" data-number="0" id="payamount">
								<i class="fa fa-money" style="color: #fff;"></i>
								<span class="pay"> Pay Amount </span>
							</a>
						</div>
					</div>
					<div class="funcbtn">
						<div class="btn-group btn-group-justified">
						  <a href="#" class="btn btn-default darkk" id="f1"><b class="pay">F1</b></a>
						  <a href="#" class="btn btn-default customm" id="f2"><b class="pay">F2</b></a>
						  <a href="#" class="btn btn-default darkk" id="f3"><b class="pay">F3</b></a>
						  <a href="#" class="btn btn-default customm" id="f4"><b class="pay">F4</b></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius: 5px !important;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close">&times;</button> -->
                    <h4 class="modal-title forquantity">Add Quantity For <b></b></h4>
                    <h4 class="modal-title forf1">F1 Product Content Here !!! <b></b></h4>
                    <h4 class="modal-title forf2">F2 Product Content Here !!! <b></b></h4>
                    <h4 class="modal-title forf3">F3 Product Content Here !!! <b></b></h4>
                    <h4 class="modal-title forf4">F4 Product Content Here !!! <b></b></h4>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-body form" style="padding-bottom: 0px !important;">
                        <div class="modal-body">
                            <form action="#" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            
                                <div class="inparea">
                                    <div class="form-group">
                                    	<div class="input-group qtystyle" style="padding-bottom: 20px;">
                                    		<span class="input-group-addon input-circle-left"> Product Barcode </span>
                                    		<input type="text" name="forbarcode" class="form-control input-circle-right" id="barcodeqty" value="" readonly="">
                                    	</div>
                                        <div class="input-group qtystyle">
                                            <input type="number" class="form-control input-circle-left" id="qtyinput" name="productquantity" value="" required="" placeholder="0" autofocus>
                                            <span class="input-group-addon input-circle-right"> Quantity </span>
                                        </div>
                                        <div class="f1prod">
                                        	<b> F1 Product Content Here !!! </b>
                                        </div>
                                        <div class="f2prod">
                                        	<b> F2 Product Content Here !!! </b>
                                        </div>
                                        <div class="f3prod">
                                        	<b> F3 Product Content Here !!! </b>
                                        </div>
                                        <div class="f4prod">
                                        	<b> F4 Product Content Here !!! </b>
                                        </div>
                                    </div>
                                    <!-- <div class="btn-set pull-left">
                                        <input id="save" type="submit" class="btn btn-default dark" value="Add Quantity" style="color: #fff !important;">
                                    </div> -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                	<!-- <button type="submit" class="btn btn-default dark" style="color: #fff !important;"> Save changes </button> -->
    				<button id="close" type="button" class="btn btn-default dark" data-dismiss="modal" style="color: #fff !important;">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal -->
</body>
</html>