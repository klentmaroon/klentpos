<form id="download-form" action="{{ route('download-pdf') }}" method="post" enctype="multipart/form-data" style="display: none;">
    {{csrf_field()}}

    <input type="text" id="filename" name="filename" value="">
    <input type="text" id="userlevel" name="userlevel" value="{{ Auth::user()->user_level }}">
    <input type="text" id="userid" name="userid" value="{{ Auth::user()->id }}">
    <input type="text" id="currentroute" name="currentroute" value="{{Route::currentRouteName()}}">

    <button type="submit">Submit</button>

</form>

<script type="text/javascript">
    jQuery(function($) {
        $(".buttons-pdf").click(function(){
            var thefilename = 'PDF - '+'{{Route::currentRouteName()}}-{{ date("Y-m-d H:i:s") }}';
            $("#filename").val(thefilename);
            event.preventDefault();
            document.getElementById('download-form').submit();
        });
        $(".buttons-excel").click(function(){
            var thefilename = 'EXCEL - '+'{{Route::currentRouteName()}}-{{ date("Y-m-d H:i:s") }}';
            $("#filename").val(thefilename);
            event.preventDefault();
            document.getElementById('download-form').submit();
        });
        $(".buttons-csv").click(function(){
            var thefilename = 'CSV - '+'{{Route::currentRouteName()}}-{{ date("Y-m-d H:i:s") }}';
            $("#filename").val(thefilename);
            event.preventDefault();
            document.getElementById('download-form').submit();
        });
    });
</script>

<!--<div class="page-footer">
    <div class="page-footer-inner">
         2014 &copy; Metronic by keenthemes.
    </div>
    <div class="page-footer-tools">
        <span class="go-top">
        <i class="fa fa-angle-up"></i>
        </span>
    </div>
</div>-->

</body>
</html>