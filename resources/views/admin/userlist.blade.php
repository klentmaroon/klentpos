@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Users
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}/admin">Admin</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/')}}/admin/users">Users</a>
                    <i class="fa fa-angle-right"></i>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        @if(session()->has('message'))
            <div class="alert alert-{{ session()->get('messageTrigger') }}">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('admin.add-user') }}" class="add-user-a"><i class="fa fa-user-plus"></i> Add User</a>

                <div class="form-group user-filterer pull-right">
                    <label class="filter-label">Users: </label>
                    <div class="col-md-9">
                        <select class="form-control" id="filterselect">
                            <option value="all">All</option>
                            <option value="auditor">Auditor</option>
                            <option value="cashier">Cashier</option>
                            <option value="user">User</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div class="row userspacer" id="user-list-details">    

            @foreach($users as $user)
                <div class="col-md-4">
                    <div class="usergrid">
                        <div class="user-ident">
                            <img src="{{ asset('userimg/userdefault.png') }}" class="userimg-default">
                        </div>
                        <div class="user-contents">
                            <div class="user-name">{{ $user->name }}</div>
                            <div class="user-email">{{ $user->email }}</div>
                            <div class="user-actions">
                                <div class="usertype-user">{{ ucwords($user->user_level) }}</div>
                                <a href="user/edit/{{ ucwords($user->id) }}" class="user-edit">Edit</a>
                                <a href="user/delete/{{ ucwords($user->id) }}" class="user-delete">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

<script type="text/javascript">
    jQuery(function($) {
        var users = JSON.parse('{!! json_encode($users) !!}');

        $("#filterselect").change(function () {
            var filterselect = $("#filterselect option:selected").val();
            $("#user-list-details").html('');
            var imgsrc = "{{ asset('userimg/userdefault.png') }}";

                if(filterselect != "all"){
                    $.each(users, function (index, value) {
                        if (value['user_level'] == filterselect) {
                            var userlevelcapitalized = value["user_level"];
                            userlevelcapitalized = userlevelcapitalized.charAt(0).toUpperCase() + userlevelcapitalized.substr(1);

                            $('#user-list-details').append('<div class="col-md-4"><div class="usergrid"><div class="user-ident"><img src="'+imgsrc+'" class="userimg-default"></div><div class="user-contents"><div class="user-name">'+value["name"]+'</div><div class="user-email">'+value["email"]+'</div><div class="user-actions"><div class="usertype-user">'+userlevelcapitalized+'</div><a href="user/edit/'+value['id']+'" class="user-edit">Edit</a>&nbsp;<a href="user/delete/'+value['id']+'" class="user-delete">Delete</a></div></div></div></div>');
                        }
                    });
                }else{
                    $.each(users, function (index, value) {
                        var userlevelcapitalized = value["user_level"];
                        userlevelcapitalized = userlevelcapitalized.charAt(0).toUpperCase() + userlevelcapitalized.substr(1);
                        $('#user-list-details').append('<div class="col-md-4"><div class="usergrid"><div class="user-ident"><img src="'+imgsrc+'" class="userimg-default"></div><div class="user-contents"><div class="user-name">'+value["name"]+'</div><div class="user-email">'+value["email"]+'</div><div class="user-actions"><div class="usertype-user">'+userlevelcapitalized+'</div><a href="user/edit/'+value['id']+'" class="user-edit">Edit</a>&nbsp;<a href="user/delete/'+value['id']+'" class="user-delete">Delete</a></div></div></div></div>');
                    });
                }
        });
    });
</script>

@endsection	