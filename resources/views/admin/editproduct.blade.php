@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')
<style>
    .portlet.light .dataTables_wrapper .dt-buttons {display: none;}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Edit Product
        </h3>
        <div class="portlet light bordered">
            <a href="{{ URL::previous() }}" class="btn btn-outline btn-circle dark btn-sm yellow"> Back </a>
        </div>
        
        @if(session()->has('message'))
            <div class="alert alert-{{ session()->get('messageTrigger') }}">
                {{ session()->get('message') }}
            </div>
        @endif
        <!-- END PAGE HEADER-->
        
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-5">
                <form action="{{url('/admin/product/update')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer"></i>
                                <span class="caption-subject uppercase"> Edit Product Form </span>
                            </div>
                        </div>
                        <div class="portlet-body form" style="padding-bottom: 0px !important;">
                            
                            @foreach($product as $value)
                                <input type="hidden" name="productid" value="{{ $value->id }}">

                                <div class="form-body">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
                                                <img src="{{ asset( '/'.$value->image ) }}" alt="">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                            <div>
                                                <span class="btn default btn-file btn-circle green">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="productimage"> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-circle-left" name="productname" value="{{ $value->name }}" required>
                                            <span class="input-group-addon input-circle-right">Name</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-circle-left" name="productbarcode" value="{{ $value->barcode }}" style="text-transform: uppercase;" required>
                                            <span class="input-group-addon input-circle-right">Bar Code</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-circle-left" name="productsku" value="{{ $value->sku }}" style="text-transform: uppercase;" required>
                                            <span class="input-group-addon input-circle-right">SKU</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-circle-left" name="productasin" value="{{ $value->asin }}" required>
                                            <span class="input-group-addon input-circle-right">ASIN</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <textarea class="form-control input-circle-left" name="productdesc" required>{{ $value->description }}</textarea>
                                            <span class="input-group-addon input-circle-right">Description</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <input type="submit" class="btn btn-circle btn-danger" value="Update">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-7">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption" style="padding-top: 15px !important;">
                            <span style="font-size: 18px !important;">List of all Quantity Per Date</span>
                        </div>
                        <div class="tools">
                            <a href="#" id="forModal" class="btn btn-outline btn-circle btn-sm purple" style="height: 30px !important;">
                                <i class="fa fa-plus-circle"></i>
                                Add Quantity
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                            <thead>
                                <tr>
                                    <td style="text-align: center; width: 10%; font-size: 10px;">Date</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Quantity">QTY</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Price">PRC</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Original Price">OPRC</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Sale Price">SPRC</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Wholesale Price">WSPRC</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($product_quantity as $value)
                                <tr>
                                    <td style="text-align: center; font-size: 10px;">{{ $value->created_at }}</td>
                                    <td style="text-align: center; font-size: 10px;">{{ $value->quantity }}</td>
                                    <td style="text-align: center; width: 100%; font-size: 10px;"> $ {{ $value->price }}</td>
                                    <td style="text-align: center; font-size: 10px;"> $ {{ $value->original_price }}</td>
                                    <td style="text-align: center; font-size: 10px;"> $ {{ $value->sale_price }}</td>
                                    <td style="text-align: center; font-size: 10px;"> $ {{ $value->wholesale_price }}</td>
                                    <td style="text-align: center; width: 25%; font-size: 10px;">
                                        <a href="{{url('/')}}/admin/product/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                        <a href="{{url('/')}}/admin/product/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue" style="margin-top: 10px;"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content" style="border-radius: 5px !important;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                @foreach($product as $value)
                                <h4 class="modal-title">Add Quantity For <b>{{ $value->name }}</b></h4>
                                @endforeach
                            </div>
                            <div class="portlet light bordered">
                                <div class="portlet-body form" style="padding-bottom: 0px !important;">
                                    <div class="modal-body">
                                        <form action="{{url('/admin/product/quantity/save')}}" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        
                                            <div style="padding-bottom: 20px;">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="number" class="form-control input-circle-left" name="productquantity" value="" required="" placeholder="0">
                                                        <span class="input-group-addon input-circle-right">Quantity</span>
                                                    </div>
                                                </div>
                                                @foreach($product as $value)
                                                    <input type="hidden" name="prodid" value="{{ $value->id }}">
                                                    @break
                                                @endforeach
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="number" class="form-control input-circle-left" name="productprice" value="" required="">
                                                            <span class="input-group-addon input-circle-right">Price</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="number" class="form-control input-circle-left" name="productorigprice" value="" required="">
                                                            <span class="input-group-addon input-circle-right">Original Price</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="number" class="form-control input-circle-left" name="productsaleprice" value="" required="">
                                                            <span class="input-group-addon input-circle-right">Sale Price</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="number" class="form-control input-circle-left" name="productwholesaleprice" value="">
                                                            <span class="input-group-addon input-circle-right">Wholesale Price</span>
                                                        </div>
                                                    </div>
                                                <div class="btn-set pull-left">
                                                    <input id="save" type="submit" class="btn btn-outline btn-circle dark btn-sm black" value="Add Quantity">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of modal -->
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection 