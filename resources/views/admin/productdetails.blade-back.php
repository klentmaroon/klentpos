@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <a href="{{ URL::previous() }}" class="btn btn-outline btn-circle dark btn-sm yellow"> Back </a>
                            <a href="" class="btn btn-outline btn-circle dark btn-sm blue"> Edit All </a>
                        </div>
                        <div class="tools" style="color: #b1b1b1;">
                            <span style="font-size: 18px !important;">Product Details</span>
                            <i class="fa fa-th-list"></i>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                            <thead>
                                <tr>
                                    <td style="text-align: center; width: 10% !important;">Thumbnail</td>
                                    <td style="text-align: center; width: 20% !important;">Name</td>
                                    <td style="text-align: center;">Description</td>
                                    <td style="text-align: center;">SKU</td>
                                    <td style="text-align: center;">ASIN</td>
                                    <td style="text-align: center; width: 10%;">Quantity</td>
                                    <!-- <td style="text-align: center; width: 10%;">Date</td> -->
                                    <td style="text-align: center; width: 15% !important;">Action</td>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($viewprod as $product)
                                <tr>
                                    <td class="proalign" style="width: 10% !important;"><img src="{{ asset('public/'. $product->image) }}" class="productsimg"></td>
                                    <td style="text-align: center; width: 20% !important;">{{ ucwords($product->name) }}</td>
                                    <td style="text-align: center;">{{ ucfirst($product->description) }}</td>
                                    <td style="text-transform: uppercase; text-align: center;">{{ ucwords($product->sku) }}</td>
                                    <td style="text-align: center;">{{ ucwords($product->asin) }}</td>
                                    <td style="text-align: center; width: 10%;">{{ $product->quantity }}</td>
                                    <td style="text-align: center;width: 15% !important;">
                                        <a href="{{url('/')}}/admin/products/edit/{{ $product->id }}" class="btn btn-outline btn-circle dark btn-sm black">Edit</a>|
                                        <a href="{{url('/')}}/admin/products/delete/{{ $product->id }}" class="btn btn-outline btn-circle red btn-sm blue">Delete</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <!-- <table class="table table-striped table-bordered table-advance table-hover">
                            <tbody>
                                @foreach($viewprod as $value)
                                <tr>
                                    <td style="text-align: center; width: 50%;"><b> Date </b></td>
                                    <td style="text-align: center; width: 50%;"><b> {{ $value->created_at }} </b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; width: 10%;">Quantity</td>
                                    <td style="text-align: center; width: 10%;"> {{ $value->quantity }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; width: 20%;">Price</td>
                                    <td style="text-align: center; width: 10%;"> $ {{ $value->price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">Original Price</td>
                                    <td style="text-align: center;"> $ {{ $value->original_price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">Sale Price</td>
                                    <td style="text-align: center;"> $ {{ $value->sale_price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">Wholesale Price</td>
                                    <td style="text-align: center;"> $ {{ $value->wholesale_price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	