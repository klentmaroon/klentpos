@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Test Form for Sales
        </h3>
        <div class="portlet light bordered">
            <a href="{{ URL::previous() }}" class="btn btn-outline btn-circle btn-sm yellow"> Back </a>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        @if(session()->has('message'))
            <div class="alert alert-{{ session()->get('messageTrigger') }}">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('/admin/sales/add/submit')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <?php 
                        /*Transaction Code*/
                        $tccode1 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $tclength1 = 4;
                        $tcstring1 = substr(str_shuffle(str_repeat($tccode1, 5)), 0, $tclength1);

                        $tccode2 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $tclength2 = 5;
                        $tcstring2 = substr(str_shuffle(str_repeat($tccode2, 5)), 0, $tclength2);

                        $tccode3 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $tclength3 = 6;
                        $tcstring3 = substr(str_shuffle(str_repeat($tccode3, 5)), 0, $tclength3);

                        $tcstring = $tcstring1."-".$tcstring2."-".$tcstring3;

                        /*Receipt Number*/
                        $rcno1 = '0123456789';
                        $rcnolength1 = 2;
                        $rcnostring1 = substr(str_shuffle(str_repeat($rcno1, 5)), 0, $rcnolength1);

                        $rcno2 = '0123456789';
                        $rcnolength2 = 3;
                        $rcnostring2 = substr(str_shuffle(str_repeat($rcno2, 5)), 0, $rcnolength2);

                        $rcno3 = '0123456789';
                        $rcnolength3 = 5;
                        $rcnostring3 = substr(str_shuffle(str_repeat($rcno3, 5)), 0, $rcnolength3);

                        $rcstring = $rcnostring1."-".$rcnostring2."-".$rcnostring3;
                    ?>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer font-red-sunglo"></i>
                                <span class="caption-subject font-red-sunglo bold uppercase"> New Sales Form </span>
                            </div>
                        </div>
                        <div class="portlet-body form" style="padding-bottom: 0px !important;">
                            <div class="form-body">

                                <div class="form-group">
                                    <label>Product</label>
                                    <select class="form-control" name="productchoice" id="productchoice">
                                        @foreach($products as $product)
                                            <option datacode="{{ $product->barcode }}" value="p-{{ $product->id }}">{{ $product->name }} - ${{ $product->price }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Cashier</label>
                                    <select class="form-control" name="userid">
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <input type="hidden" name="barcode" id="barcode" value="">

                        <script type="text/javascript">
                            jQuery(function($) {
                                $(document).ready(function(){

                                    var productchoice = $('#productchoice option:selected').attr('datacode');
                                    $('#barcode').val(productchoice);

                                    $('#productchoice').change(function(){
                                        var productchoice = $('#productchoice option:selected').attr('datacode');
                                        $('#barcode').val(productchoice);
                                    })

                                });
                            });
                        </script>

                                <br>
                                <br>
                                <br>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-circle-left" name="txcode" value="<?php echo 'TC-'.$tcstring; ?>" maxlength="20" required="" readonly="">
                                        <span class="input-group-addon input-circle-right">Transaction Code</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-circle-left" name="receiptno" value="<?php echo 'RC-'.$rcstring; ?>" maxlength="20" required="" readonly="">
                                        <span class="input-group-addon input-circle-right">Receipt Number</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-circle-left" name="sukicardno" value="" placeholder="SUKI-012-3456-789" required="">
                                        <span class="input-group-addon input-circle-right">Suki Card Number</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="quantity" name="quantity" value="" required="" placeholder="0" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Quantity</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="price" name="price" value="" required="" placeholder="$00.00" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="totalpur" name="totalpur" value="" required="" placeholder="$00.00" step="any" min="0" readonly="">
                                        <span class="input-group-addon input-circle-right">Total Purchase</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="money" name="money" value="" required="" placeholder="$00.00" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Money</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="change" name="change" value="" required="" placeholder="$00.00" step="any" min="0" readonly="">
                                        <span class="input-group-addon input-circle-right">Change</span>
                                    </div>
                                </div>
                                <div class="form-actions" style="padding-bottom: 0px !important;">
                                    <div class="btn-set pull-right">
                                        <input type="submit" class="btn btn-circle btn-danger" value="Add New Sales">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!-- <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script> -->
                <script type="text/javascript">
                    $(document).ready(function(){ 
                        $('#quantity').on('input', function() {
                          calc1 = calculate();
                        });
                        $('#price').on('input', function() {
                          calc2 = calculate();
                        });
                        function calculate(){
                            var quantity = parseFloat($('#quantity').val()); 
                            var price = parseFloat($('#price').val());
                            var calculatedtotalpur="";

                            if(isNaN(quantity) || isNaN(price)){
                              calculatedtotalpur=" ";
                            }else{
                              calculatedtotalpur = ( quantity*price ).toFixed(2);
                            }
                            
                            $('#totalpur').val(calculatedtotalpur);
                        }

                        $('#money').on('input', function(){
                            sub = calculate1();
                        });
                        function calculate1() {
                            var totalpurchase = parseFloat($('#totalpur').val());
                            var money = parseFloat($('#money').val());
                            var calculatedchange="";

                            if(isNaN(totalpurchase) || isNaN(money)){
                              calculatedchange=" ";
                            }else{
                              calculatedchange = ( money-(totalpurchase) ).toFixed(2);
                            }
                            
                            $('#change').val(calculatedchange);
                        }
                    });
                </script>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	