@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Products
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{url('/')}}/admin/addproduct">Add New</a>
                </li>
                &nbsp;|&nbsp;
                <li>
                    <a href="{{url('/')}}/admin/uploadcsv">Upload CSV</a>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="main-title"><span>CSV Importer</span></div>
                <form class="upload-form" action="{{url('/admin/uploadcsv-submit')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-title">Select Your Csv File</div>
                    <div class="form-file"><input name="csvfile" type="file"></div>
                    <div class="form-file-submit">
                        <input type="submit" name="csvupload" value="Upload" class="csv-btn">
                    </div>
                </form>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	