@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Edit Product Quantity
        </h3>
        <div class="portlet light bordered">
            <a href="{{ URL::previous() }}" class="btn btn-outline btn-circle dark btn-sm yellow"> Back </a>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-6">
                <form action="{{url('/admin/product/quantity/update')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer font-red-sunglo"></i>
                                <span class="caption-subject font-red-sunglo bold uppercase"> Edit Quantity Form </span>
                            </div>
                        </div>
                        <div class="portlet-body form" style="padding-bottom: 0px !important;">
                            @foreach($product_quantity as $value)
                                <input type="hidden" name="productquantityid" value="{{ $value->id }}">
                                <input type="hidden" name="product_id" value="{{ $value->product_id }}">
                                
                                <!-- <div class="inp dat5" style="margin-bottom: 10px;">
                                    <span>Quantity: </span>
                                    <input type="number" name="productquantity" value="{{ $value->quantity }}" style="width: 100px !important; height: 30px;">
                                </div>
 -->
                                <!-- <div class="inp dat3"><span>Price: </span>
                                    <input required type="number" name="productqprice" placeholder="$0.00" value="{{ $value->price }}">
                                </div> -->

                                <!-- <div class="inp dat3"><span>Original Price: </span>
                                    <input required type="number" name="productqorigprice" placeholder="$0.00" value="{{ $value->original_price }}">
                                </div> -->

                                <!-- <div class="inp dat3"><span>Sale Price: </span>
                                    <input required type="number" name="productqsaleprice" placeholder="$0.00" value="{{ $value->sale_price }}">
                                </div> -->

                                <!-- <div class="inp dat3"><span>Wholesale Price: </span>
                                    <input required type="number" name="productqwholesaleprice" placeholder="$0.00" value="{{ $value->wholesale_price }}">
                                </div> -->

                                <!-- --------------------------------------------- -->

                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productquantity" value="{{ $value->quantity }}" required="" placeholder="0">
                                        <span class="input-group-addon input-circle-right">Quantity</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productqprice" value="{{ $value->price }}" required="" placeholder="$00.00" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productqorigprice" value="{{ $value->original_price }}" required="" placeholder="$00.00" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Original Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productqsaleprice" value="{{ $value->sale_price }}" required="" placeholder="$00.00" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Sale Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productqwholesaleprice" value="{{ $value->wholesale_price }}" required="" placeholder="$00.00" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Wholesale Price</span>
                                    </div>
                                </div>
                                <div class="form-actions" style="padding-bottom: 0px !important;">
                                    <div class="btn-set pull-right">
                                        <input id="save" type="submit" class="btn btn-circle btn-danger" value="Update Product Quantity">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection 