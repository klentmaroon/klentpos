@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Edit Product
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{url('/')}}/admin/products"><i class="fa fa-angle-double-left">&nbsp;Back</i></a>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-6">
                <form action="{{url('/admin/products/update')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer"></i>
                                <span class="caption-subject uppercase"> Edit Product Form </span>
                            </div>
                        </div>
                        <div class="portlet-body form" style="padding-bottom: 0px !important;">
                            @foreach($product as $value)
                                <input type="hidden" name="productid" value="{{ $value->id }}">
                                <!-- <div class="file-wrapper">
                                    <input required id="animage" type="file" name="productimage" onchange="loadFile(event)">
                                </div>
                                <div class="prev-file">
                                    <img id="show-image" class="prev-image" src="{{ asset( 'public/'.$value->image ) }}">
                                </div> -->

                                <div class="form-body">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
                                                <img src="{{ asset( 'public/'.$value->image ) }}" alt="">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                            <div>
                                                <span class="btn default btn-file btn-circle green">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="productimage"> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-circle-left" name="productname" value="{{ $value->name }}">
                                            <span class="input-group-addon input-circle-right">Name</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-circle-left" name="productsku" value="{{ $value->sku }}" style="text-transform: uppercase;">
                                            <span class="input-group-addon input-circle-right">SKU</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-circle-left" name="productasin" value="{{ $value->asin }}">
                                            <span class="input-group-addon input-circle-right">ASIN</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <textarea class="form-control input-circle-left" name="productdesc">{{ $value->description }}</textarea>
                                            <span class="input-group-addon input-circle-right">Description</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <input type="submit" class="btn btn-circle btn-danger" value="Update">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-6">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption" style="padding-top: 15px !important;">
                            <span style="font-size: 18px !important;">List of all Quantity Per Date</span>
                        </div>
                        <div class="tools">
                            <a href="#" id="forModal" class="btn btn-outline btn-circle btn-sm purple" style="height: 30px !important;">
                                <i class="fa fa-plus-circle"></i>
                                Add Quantity
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                            <thead>
                                <tr>
                                    <td style="text-align: center; width: 100%;">Date</td>
                                    <td style="text-align: center;">Quantity</td>
                                    <td style="text-align: center; width: 100%;">Price</td>
                                    <td style="text-align: center;">Original Price</td>
                                    <td style="text-align: center;">Sale Price</td>
                                    <td style="text-align: center;">Wholesale Price</td>
                                    <td style="text-align: center; width: 25% !important;">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($productquants as $value)
                                <tr>
                                    <td style="text-align: center;">{{ $value->created_at }}</td>
                                    <td style="text-align: center;">{{ $value->quantity }}</td>
                                    <td style="text-align: center; width: 100%;"> $ {{ $value->price }}</td>
                                    <td style="text-align: center;"> $ {{ $value->original_price }}</td>
                                    <td style="text-align: center;"> $ {{ $value->sale_price }}</td>
                                    <td style="text-align: center;"> $ {{ $value->wholesale_price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                        &nbsp;
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @foreach($productquants as $value)
                        <!-- <table class="table table-striped table-bordered table-advance table-hover">
                            <tbody>
                                <tr>
                                    <td style="text-align: center; width: 50%;"><b> Date </b></td>
                                    <td style="text-align: center; width: 50%;"><b> {{ $value->created_at }} </b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; width: 10%;">Quantity</td>
                                    <td style="text-align: center; width: 10%;"> {{ $value->quantity }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; width: 20%;">Price</td>
                                    <td style="text-align: center; width: 10%;"> $ {{ $value->price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">Original Price</td>
                                    <td style="text-align: center;"> $ {{ $value->original_price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">Sale Price</td>
                                    <td style="text-align: center;"> $ {{ $value->sale_price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">Wholesale Price</td>
                                    <td style="text-align: center;"> $ {{ $value->wholesale_price }}</td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/edit/{{ $value->id }}" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td style="text-align: center; width: 10%;">
                                        <a href="{{url('/')}}/admin/products/quantity/delete/{{ $value->id }}" class="btn btn-outline btn-circle red btn-sm blue"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table> -->
                    @endforeach
                    </div>
                </div>
                    <!-- <h5 class="text-center" style="margin-bottom: 25px;"><b>List of all Quantity Per Date</b></h4> -->

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content" style="border-radius: 5px !important;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    @foreach($product as $value)
                                    <h4 class="modal-title">Add Quantity For <b>{{ $value->name }}</b></h4>
                                    @endforeach
                                </div>
                                <div class="portlet light bordered">
                                    <div class="portlet-body form" style="padding-bottom: 0px !important;">
                                        <div class="modal-body">
                                            <form action="{{url('/admin/products/quantity/save')}}" method="post" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            
                                                <div style="padding-bottom: 20px;">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="number" class="form-control input-circle-left" name="productquantity" value="" required="" placeholder="0">
                                                            <span class="input-group-addon input-circle-right">Quantity</span>
                                                        </div>
                                                    </div>
                                                    @foreach($productquants as $value)
                                                        <input type="hidden" name="prodid" value="{{ $value->product_id }}">

                                                        <div class="inp dat3" hidden=""><span>Price: </span>
                                                            <input required type="number" name="productprice" placeholder="$0.00" value="{{ $value->price }}">
                                                        </div>

                                                        <div class="inp dat3" hidden=""><span>Original Price: </span>
                                                            <input required type="number" name="productorigprice" placeholder="$0.00" value="{{ $value->original_price }}">
                                                        </div>

                                                        <div class="inp dat3" hidden=""><span>Sale Price: </span>
                                                            <input required type="number" name="productsaleprice" placeholder="$0.00" value="{{ $value->sale_price }}">
                                                        </div>

                                                        <div class="inp dat3" hidden=""><span>Wholesale Price: </span>
                                                            <input required type="number" name="productwholesaleprice" placeholder="$0.00" value="{{ $value->wholesale_price }}">
                                                        </div>
                                                    @endforeach
                                                    <!-- <input id="save" class="btn btn-default" type="submit" value="Save" style="width: 100px !important;"> -->
                                                    <div class="btn-set pull-left">
                                                        <input id="save" type="submit" class="btn btn-outline btn-circle dark btn-sm black" value="Add Quantity">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of modal -->

                    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

                    <script type="text/javascript">
                        $(document).ready(function(){
                            $("#forModal").click(function(){
                                $("#myModal").modal();
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection 