@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Edit Sales
        </h3>
        <div class="portlet light bordered">
            <a href="{{ URL::previous() }}" class="btn btn-outline btn-circle btn-sm yellow"> Back </a>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        @if(session()->has('message'))
            <div class="alert alert-{{ session()->get('messageTrigger') }}">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <form action="{{url('/admin/sales/update')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer"></i>
                                <span class="caption-subject uppercase"> Edit Sales Form </span>
                            </div>
                        </div>
                        <div class="portlet-body form" style="padding-bottom: 0px !important;">
                            @foreach($sale as $sales)
                            <div class="form-body">
                                <input type="hidden" name="salesid" id="salesid" value="{{$sales->id}}">
                                <input type="hidden" name="userid" value="{{ $sales->user_id }}">

                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="quantity" name="quantity" value="" required="" placeholder="0" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Quantity</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="price" name="price" value="" required="" placeholder="$00.00" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="totalpur" name="totalpur" value="{{$sales->total_purchase}}" required="" placeholder="$00.00" step="any" min="0" readonly="">
                                        <span class="input-group-addon input-circle-right">Total Purchase</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="money" name="money" value="{{$sales->money}}" required="" placeholder="$00.00" step="any" min="0">
                                        <span class="input-group-addon input-circle-right">Money</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" id="change" name="change" value="{{$sales->change}}" required="" placeholder="$00.00" step="any" min="0" readonly="">
                                        <span class="input-group-addon input-circle-right">Change</span>
                                    </div>
                                </div>
                                <div class="form-actions" style="padding-bottom: 0px !important;">
                                    <div class="btn-set pull-right">
                                        <input type="submit" class="btn btn-circle btn-danger" value="Update Sales">
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </form>

                <!-- <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script> -->
                <script type="text/javascript">
                    $(document).ready(function(){ 
                        $('#quantity').on('input', function() {
                          calc1 = calculate();
                        });
                        $('#price').on('input', function() {
                          calc2 = calculate();
                        });
                        function calculate(){
                            var quantity = parseFloat($('#quantity').val()); 
                            var price = parseFloat($('#price').val());
                            var calculatedtotalpur="";

                            if(isNaN(quantity) || isNaN(price)){
                              calculatedtotalpur=" ";
                            }else{
                              calculatedtotalpur = ( quantity*price ).toFixed(2);
                            }
                            
                            $('#totalpur').val(calculatedtotalpur);
                        }

                        $('#money').on('input', function(){
                            sub = calculate1();
                        });
                        function calculate1() {
                            var totalpurchase = parseFloat($('#totalpur').val());
                            var money = parseFloat($('#money').val());
                            var calculatedchange="";

                            if(isNaN(totalpurchase) || isNaN(money)){
                              calculatedchange=" ";
                            }else{
                              calculatedchange = ( money-(totalpurchase) ).toFixed(2);
                            }
                            
                            $('#change').val(calculatedchange);
                        }
                    });
                </script>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection 