<div class="page-sidebar-wrapper sidebar-custom">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <?php
                $dashboard = "";
                $products = "";
                $orders = "";
                $sales = "";
                $users = "";
                $settings = "";
                if(Route::currentRouteName() == "admin-dashboard"){$dashboard = "active";}
                elseif(Route::currentRouteName() == "admin-product" || Route::currentRouteName() == "admin-uploadcsv" || Route::currentRouteName() == "admin-new-product" || Route::currentRouteName() == "admin-topsaleable-product" || Route::currentRouteName() == "admin-nonmoving-product" || Route::currentRouteName() == "admin-view-product" || Route::currentRouteName() == "admin-edit-product"){$products = "active";}
                elseif(Route::currentRouteName() == "admin-orders"){$orders = "active";}
                elseif(Route::currentRouteName() == "admin-sales" || Route::currentRouteName() == "admin-add-sales" || Route::currentRouteName() == "admin-edit-sales"){$sales = "active";}
                elseif(Route::currentRouteName() == "admin-userlist" || Route::currentRouteName() == "admin.add-user" || Route::currentRouteName() == "admin.edit-user" || Route::currentRouteName() == "admin.top-performers-user"){$users = "active";}
                elseif(Route::currentRouteName() == "admin.reports"){$reports = "active";}
                elseif(Route::currentRouteName() == "admin-settings"){$settings = "active";}
            ?>
            
            <li class="start {{$dashboard}}">
                <a href="{{url('/')}}/admin/dashboard">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
                </a>
            </li>
            <li class="start {{$products}} submenu-position" id="tab-products">
                <a class="product-tab">
                <i class="fa fa-location-arrow"></i>
                <span class="title">Products</span>
                <span class="selected"></span>
                </a>
                    <ul class="product-submenu page-sidebar-menu" id="submenu-prods">
                        <li class="">
                            <a href="{{url('/')}}/admin/products">
                            <span>All Products</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('admin-topsaleable-product') }}">
                            <span>Top Saleable Products</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('admin-nonmoving-product') }}">
                            <span>Non Moving Products</span>
                            </a>
                        </li>
                    </ul>

            </li>
            <li class="start {{$orders}}">
                <a href="{{url('/')}}/admin/orders">
                <i class="fa fa-shopping-cart"></i>
                <span class="title">Orders</span>
                <span class="selected"></span>
                </a>
            </li>
            <li class="start {{$sales}}">
                <a href="{{url('/')}}/admin/sales">
                <i class="fa fa-pie-chart"></i>
                <span class="title">Sales</span>
                <span class="selected"></span>  
                </a>
            </li>
            <li class="start {{$users}} submenu-position" id="tab-users">
                <a class="users-tab">
                <i class="fa fa-user"></i>
                <span class="title">Users</span>
                <span class="selected"></span>
                </a>

                    <ul class="users-submenu page-sidebar-menu" id="submenu-users">
                        <li class="">
                            <a href="{{url('/')}}/admin/users">
                            <span>All Users</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('admin.top-performers-user') }}">
                            <span>Cashier Top Performers</span>
                            </a>
                        </li>
                    </ul>
            </li>
            <li class="start {{$reports}}">
                <a href="{{ route('admin.reports') }}">
                <i class="fa fa-book"></i>
                <span class="title">Reports</span>
                <span class="selected"></span>
                </a>
            </li>
            <li class="start {{$settings}}">
                <a href="{{url('/')}}/admin/settings">
                <i class="fa fa-cogs"></i>
                <span class="title">Settings</span>
                <span class="selected"></span>
                </a>
            </li>
        </ul>

        <script type="text/javascript">
            jQuery(function($) {
                var x = document.getElementById("submenu-prods");
                var y = document.getElementById("submenu-users");

                $("#tab-products").click(function(){
                    if (x.style.display === "block") {
                        x.style.display = "none";
                    } else {
                        x.style.display = "block";
                    }
                });

                $("#tab-users").click(function(){
                    if (y.style.display === "block") {
                        y.style.display = "none";
                    } else {
                        y.style.display = "block";
                    }
                });
            });
        </script>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->