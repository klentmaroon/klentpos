@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Product Details
        </h3>
        <div class="portlet light bordered">
            <a href="{{ URL::previous() }}" class="btn btn-outline btn-circle dark btn-sm yellow"> Back </a>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-5">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div style="text-align: center; color: #666; padding: 10px 0; font-size: 18px; line-height: 18px;">
                            <span class="caption-subject uppercase"> Product Details </span>
                        </div>
                    </div>
                    <div class="portlet-body form" style="padding-bottom: 0px !important;">
                            <input type="hidden" name="productid" value="{{ $product->id }}">

                            <div class="form-body" style="text-align: center;">
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
                                            <img src="{{ asset( '/'.$product->image ) }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left" style="border: none;">Name</span>
                                        <input type="text" class="form-control input-circle-right" name="productsku" value="{{ $product->name }}" style="border: none;" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left" style="border: none;">Bar Code</span>
                                        <input type="text" class="form-control input-circle-right" name="productbarcode" value="{{ $product->barcode }}" style="text-transform: uppercase; border: none;" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left" style="border: none;">SKU</span>
                                        <input type="text" class="form-control input-circle-right" name="productsku" value="{{ $product->sku }}" style="text-transform: uppercase; border: none;" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left" style="border: none;">ASIN</span>
                                        <input type="text" class="form-control input-circle-right" name="productasin" value="{{ $product->asin }}" style="border: none;" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left" style="border: none;">Description</span>
                                        <textarea class="form-control input-circle-right" name="productdesc" style="border: none;" readonly="">{{ $product->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption" style="padding-top: 15px !important;">
                            <span style="font-size: 18px !important;">List of all Quantity Per Date</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                            <thead>
                                <tr>
                                    <td style="text-align: center; width: 10%; font-size: 10px;">Date</td>
                                    <td style="text-align: center; width: 10% !important; font-size: 10px;" title="Quantity">QTY</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Price">PRC</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Original Price">OPRC</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Sale Price">SPRC</td>
                                    <td style="text-align: center; width: 10%; font-size: 10px;" title="Wholesale Price">WSPRC</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($product->quantity as $value)
                                <tr>
                                    <td style="text-align: center; font-size: 10px;">{{ $value->created_at }}</td>
                                    <td style="text-align: center; font-size: 10px;">{{ $value->quantity }}</td>
                                    <td style="text-align: center; width: 10% !important; font-size: 10px;"> $ {{ $value->price }}</td>
                                    <td style="text-align: center; font-size: 10px;"> $ {{ $value->original_price }}</td>
                                    <td style="text-align: center; font-size: 10px;"> $ {{ $value->sale_price }}</td>
                                    <td style="text-align: center; font-size: 10px;"> $ {{ $value->wholesale_price }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection