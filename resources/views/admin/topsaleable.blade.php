@extends('Admin.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-location-arrow" style="font-size: 18px !important; margin-top: 7px !important;"></i>
                            <span>Top Saleable Products</span>&nbsp;
                            <a href="{{url('/')}}/admin/product/add" class="btn btn-outline btn-circle btn-sm yellow">Add New Product</a>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                            <!-- <table class="table table-hover table-light"> -->
                            <thead>
                                <tr>
                                    <td style="text-align: center; width: 10%;"></td>
                                    <td style="text-align: center; width: 10%;">Barcode</td>
                                    <td style="text-align: center; width: 20%;">Name</td>
                                    <td style="text-align: center;">Price</td>
                                    <td style="text-align: center;">Quantity Purchased</td>
                                    <td style="text-align: center; width: 25% !important;">Action</td>
                                </tr>
                            </thead>
                            <tbody>

                                @if( $products != false )
                                <?php foreach($products as $product => $value) { ?>
                                <tr>
                                    <td class="proalign" style="width: 10%;"><img src="{{ asset('/'. $value['image']) }}" class="productsimg"></td>
                                    <td style="width: 20%;">{{ ucwords($value['barcode']) }}</td>
                                    <td style="width: 20%;">{{ ucwords($value['name']) }}</td>
                                    <td style="text-align: center;">{{ '$'. $value['price'] }}</td>
                                    <td style="text-align: center;">{{ $value['count'] }}</td>
                                    <td style="text-align: center; width: 25% !important;">
                                        <a href="{{url('/')}}/admin/products/details/{{ $value['product_id'] }}" class="btn btn-outline btn-circle btn-sm purple">View</a>|
                                        <a href="{{url('/')}}/admin/products/edit/{{ $value['product_id'] }}" class="btn btn-outline btn-circle dark btn-sm black">Edit</a>|
                                        <a href="{{url('/')}}/admin/products/delete/{{ $value['product_id'] }}" class="btn btn-outline btn-circle red btn-sm blue">Delete</a></td>
                                </tr>
                                <?php } ?>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	