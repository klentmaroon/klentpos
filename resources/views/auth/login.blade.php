@extends('layouts.custom-layout')

@section('content')
<div class="container loginc">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- <div class="panel panel-default">
                <div class="panel-heading">Login</div> -->

            <div class="panel-body">
                <h3 class="admintit">Sign In Here</h3>
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                        <div class="col-md-8 col-md-offset-2">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

                        <div class="col-md-8 col-md-offset-2">
                            <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="checkbox">
                                <label>
                                    <input id="rem" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="remem">Remember Me</span>
                                </label>
                            </div>
                            <div class="forget">
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    <span class="for"> Forgot Your Password? </span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-default" id="ctbtn">
                                Login
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- </div>
        </div> -->
    </div>
</div>
@endsection

<style type="text/css">
    body {
        background-image: url("{{url('/')}}/assets/apps/img/login-bg-1.jpg");
        background-repeat: no-repeat; 
    }
    .loginc {
        padding: 80px;
    }
    .admintit {
        color: #a7b7ba;
        text-align: center;
        padding-bottom: 20px;
    }
    #email {
        background-color: rgba(67,34,167,.4) !important;
        border-radius: 40px;
        height: 50px;
        font-size: 17px;
        padding: 16px 10px 15px 35px;
        font-family: 'Quicksand', sans-serif;
        color: #a7b7ba;
    }
    #password {
        background-color: rgba(67,34,167,.4) !important;
        border-radius: 40px;
        height: 50px;
        font-size: 17px;
        padding: 16px 10px 15px 35px;
        font-family: 'Quicksand', sans-serif;
        color: #a7b7ba;
    }
    #rem {
        width: 20px !important;
        height: 20px !important;
        position: relative;
        left: 15px;
        bottom: 4px;
    }
    .remem {
        position: relative;
        left: 18px;
        bottom: 8px;
        color: #a7b7ba;
    }
    .checkbox {
        float: left;
    }
    .forget {
        float: right;
    }
    .for {
        color: #a7b7ba;
    }
    .for:hover {
        text-decoration: underline;
        text-decoration-color: #a7b7ba; 
        color: #ffffff;
    }
    #ctbtn {
        border-radius: 40px;
        background-color: transparent;
        height: 45px;
        width: 200px;
        color: #ffffff;
    }
    .tit {
        padding-top: 14px;
        padding-bottom: 14px;
        padding-right: 5px;
        color: #c2acf4;
        font-weight: 400;
    }
    .ctre {
        position: relative;
        display: inline-block;
        top: 14px;
    }
    .ctre > a {
        color: #ffffff;
    }
    .ctre > a:hover {
        color: #ffffff;
        text-decoration: underline;
        text-decoration-color: #a7b7ba; 
    }
</style>