@extends('Auditor.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-location-arrow" style="font-size: 18px !important; margin-top: 7px !important;"></i>
                            <span>Products</span>&nbsp;
                            <a href="{{ route('auditor-new-product') }}" class="btn btn-outline btn-circle btn-sm yellow">Add New Product</a>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                            <!-- <table class="table table-hover table-light"> -->
                            <thead>
                                <tr>
                                    <td style="text-align: center;"></td>
                                    <td style="text-align: center; width: 20%;">Code</td>
                                    <td style="text-align: center; width: 20%;">Name</td>
                                    <td style="text-align: center;">Description</td>
                                    <td style="text-align: center;">Quantity</td>
                                    <td style="text-align: center;">Price</td>
                                    <td style="text-align: center; width: 25% !important;">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td class="proalign" style="width: 10%;"><img src="{{ asset('/'. $product->image) }}" class="productsimg"></td>
                                    <td style="width: 20%;">{{ ucwords($product->barcode) }}</td>
                                    <td style="width: 20%;">{{ ucwords($product->name) }}</td>
                                    <td>{{ ucfirst($product->description) }}</td>
                                    <td style="text-align: center;">{{ $product->quantity->sum('quantity') }}</td>
                                    <td style="text-align: center;">{{ '$'. $product->quantity->max('price') }}</td>
                                    <td style="text-align: center; width: 25% !important;">
                                        <a href="{{url('/')}}/auditor/product/details/{{ $product->id }}" class="btn btn-outline btn-circle btn-sm purple">View</a>|
                                        <a href="{{url('/')}}/auditor/product/edit/{{ $product->id }}" class="btn btn-outline btn-circle dark btn-sm black">Edit</a>|
                                        <a href="{{url('/')}}/auditor/product/delete/{{ $product->id }}" class="btn btn-outline btn-circle red btn-sm blue">Delete</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	