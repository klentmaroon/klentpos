<div class="page-sidebar-wrapper sidebar-custom">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <?php
                $dashboard = "";
                $products = "";
                $sales = "";
                $reports = "";
                $settings = "";
                $users = "";
                if(Route::currentRouteName() == "auditor.dashboard"){$dashboard = "active";}
                elseif(Route::currentRouteName() == "auditor.products" || Route::currentRouteName() == "auditor-topsaleable-product" || Route::currentRouteName() == "auditor-nonmoving-product" || Route::currentRouteName() == "auditor-new-product" || Route::currentRouteName() == "auditor-view-product" || Route::currentRouteName() == "auditor-edit-product"){$products = "active";}
                elseif(Route::currentRouteName() == "auditor.sales"){$sales = "active";}
                elseif(Route::currentRouteName() == "auditor.reports"){$reports = "active";}
                elseif(Route::currentRouteName() == "admin-settings"){$settings = "active";}
                elseif(Route::currentRouteName() == "auditor.cashiers" || Route::currentRouteName() == "auditor.top-performers-user" || Route::currentRouteName() == "auditor.add-user" || Route::currentRouteName() == "auditor.edit-user"){$users = "active";}
            ?>
            
            <li class="start {{$dashboard}}">
                <a href="{{ route('auditor.dashboard') }}">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
                </a>
            </li>
            <li class="start {{$products}} submenu-position" id="tab-products">
                <a href="#">
                <i class="fa fa-location-arrow"></i>
                <span class="title">Products</span>
                <span class="selected"></span>
                </a>
                    <ul class="product-submenu page-sidebar-menu" id="submenu-prods">
                        <li class="">
                            <a href="{{ route('auditor.products') }}">
                            <span>All Products</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('auditor-topsaleable-product') }}">
                            <span>Top Saleable Products</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('auditor-nonmoving-product') }}">
                            <span>Non Moving Products</span>
                            </a>
                        </li>
                    </ul>
            </li>
            <li class="start {{$sales}}">
                <a href="{{ route('auditor.sales') }}">
                <i class="fa fa-pie-chart"></i>
                <span class="title">Sales</span>
                <span class="selected"></span>  
                </a>
            </li>
            <li class="start {{$reports}}">
                <a href="{{ route('auditor.reports') }}">
                <i class="fa fa-user"></i>
                <span class="title">Reports</span>
                <span class="selected"></span>
                </a>
            </li>
            <li class="start {{$users}} submenu-position" id="tab-users">
                <a class="users-tab">
                <i class="fa fa-user"></i>
                <span class="title">Cashiers</span>
                <span class="selected"></span>
                </a>
                    <ul class="users-submenu page-sidebar-menu" id="submenu-users">
                        <li class="">
                            <a href="{{ route('auditor.cashiers') }}">
                            <span>All Users</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('auditor.top-performers-user') }}">
                            <span>Cashier Top Performers</span>
                            </a>
                        </li>
                    </ul>
            </li>
            <li class="start {{$settings}}">
                <a href="#">
                <i class="fa fa-cogs"></i>
                <span class="title">Settings</span>
                <span class="selected"></span>
                </a>
            </li>
        </ul>
        <script type="text/javascript">
            jQuery(function($) {
                var x = document.getElementById("submenu-prods");
                var y = document.getElementById("submenu-users");

                $("#tab-products").click(function(){
                    if (x.style.display === "block") {
                        x.style.display = "none";
                    } else {
                        x.style.display = "block";
                    }
                });

                $("#tab-users").click(function(){
                    if (y.style.display === "block") {
                        y.style.display = "none";
                    } else {
                        y.style.display = "block";
                    }
                });
            });
        </script>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->