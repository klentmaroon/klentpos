@extends('Auditor.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Add Cashier
        </h3>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        @if(session()->has('message'))
            <div class="alert alert-{{ session()->get('messageTrigger') }}">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="adduser-wrapper">

                <form action="{{ route('auditor.add-user-submit') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Email Address</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input type="email" class="form-control" placeholder="Email Address" required name="useremail">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>
                            <input type="password" class="form-control" placeholder="Password" required name="userpassword">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Name</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i>N</i>
                            </span>
                            <input type="text" class="form-control" placeholder="Name" required name="username">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>User Type</label>
                        <select class="form-control" name="usertype">
                            <option value="cashier">Cashier</option>
                        </select>
                    </div>

                    <div class="form-actions" style="padding-bottom: 0px !important;">
                        <div class="btn-set pull-left">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	