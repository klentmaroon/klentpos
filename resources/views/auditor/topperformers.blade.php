@extends('Auditor.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Users
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}/admin">Admin</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/')}}/admin/users">Users</a>
                    <i class="fa fa-angle-right"></i>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('auditor.add-user') }}" class="add-user-a"><i class="fa fa-user-plus"></i> Add User</a>
            </div>
        </div>
        <div class="user-tp-grid">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                    <!-- <table class="table table-hover table-light"> -->
                    <thead>
                        <tr>
                            <td style="text-align: center; width: 20%;">...</td>
                            <td style="text-align: center; width: 20%;">Name</td>
                            <td style="text-align: center;">Email</td>
                            <td style="text-align: center;">Sales</td>
                        </tr>
                    </thead>
                    <tbody>

                    <?php foreach($users as $user => $value){ ?>
                        <tr>
                            <td class="proalign" style="width: 20%;"><img src="{{ asset('userimg/userdefault.png') }}" class="productsimg"></td>
                            <td style="text-align: center; width: 20%; vertical-align: middle;">{{ ucwords($value['name']) }}</td>
                            <td style="text-align: center; vertical-align: middle;">{{ $value['email'] }}</td>
                            <td style="text-align: center; vertical-align: middle;">{{ $value['count'] }}</td>
                        </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	