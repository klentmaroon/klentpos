@extends('Auditor.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="portlet light bordered">
            <a href="{{ URL::previous() }}" class="btn btn-outline btn-circle dark btn-sm yellow"> Back </a>
            <a href="" class="btn btn-outline btn-circle dark btn-sm yellow">Upload CSV</a>
        </div>


        @if(session()->has('message'))
            <div class="alert alert-{{ session()->get('messageTrigger') }}">
                {{ session()->get('message') }}
            </div>
        @endif
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <form action="{{url('/auditor/product/add/submit')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer font-red-sunglo"></i>
                                <span class="caption-subject font-red-sunglo bold uppercase"> Add New Product </span>
                            </div>
                        </div>
                        <div class="portlet-body form" style="padding-bottom: 0px !important;">
                            <div class="form-body col-md-3">
                                <div class="form-group" style="text-align: center !important;">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" alt="">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                        <div>
                                            <span class="btn default btn-file btn-circle green">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="productimage" required> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body col-md-offset-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-circle-left" name="productname" value="" required="" placeholder="Product Name" style="border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">Name</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-circle-left" name="productbarcode" value="" maxlength="50" required="" placeholder="Bar Code" style="border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">Bar Code</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-circle-left" name="productsku" value="" maxlength="10" required="" placeholder="Product SKU" style="text-transform: uppercase; border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">SKU</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-circle-left" name="productasin" value="" required="" placeholder="Product ASIN" style="border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">ASIN</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <textarea class="form-control input-circle-left" name="productdesc" placeholder="Product Description" style="border-radius: 0px !important;"></textarea>
                                        <span class="input-group-addon input-circle-right">Description</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productquantity" value="" required="" placeholder="0" style="border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">Quantity</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productprice" value="" required="" placeholder="$00.00" step="any" min="0" style="border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productorigprice" value="" required="" placeholder="$00.00" step="any" min="0" style="border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">Original Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productsaleprice" value="" required="" placeholder="$00.00" step="any" min="0" style="border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">Sale Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" class="form-control input-circle-left" name="productwholesaleprice" value="" required="" placeholder="$00.00" step="any" min="0" style="border-radius: 0px !important;">
                                        <span class="input-group-addon input-circle-right">Wholesale Price</span>
                                    </div>
                                </div>
                                <div class="form-actions" style="padding-bottom: 0px !important;">
                                    <div class="btn-set pull-right">
                                        <input type="submit" class="btn btn-circle btn-danger" value="Add Product">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	