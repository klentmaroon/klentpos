@extends('Auditor.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <h3 class="page-title">
            Sales
        </h3> -->
        <!-- <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}/admin">Admin</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/')}}/admin/sales/add">Add Test Sales</a>
                    <i class="fa fa-angle-right"></i>
                </li>
            </ul>
        </div> -->
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-location-arrow" style="font-size: 18px !important; margin-top: 7px !important;"></i>
                            <span>Sales</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                        <!-- <table id="products-tab"> -->
                            <thead>
                                <tr>
                                    <td style="text-align: center; width: 10% !important;">User ID</td>
                                    <td style="text-align: center; width: 10% !important;">Barcode</td>
                                    <td style="text-align: center; width: 17% !important;">Suki Card Number</td>
                                    <td style="text-align: center;">Total Purchase</td>
                                    <td style="text-align: center;">Money</td>
                                    <td style="text-align: center;">Change</td>
                                    <td style="text-align: center;">Transaction Code</td>
                                    <td style="text-align: center;">Receipt Number</td>
                                    <!-- <td style="text-align: center; width: 15% !important;">Action</td> -->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sales as $sale)
                                    <tr>
                                        <td style="width: 10% !important; text-align: center;">{{ $sale->user_id }}</td>
                                        <td style="width: 10% !important; text-align: center;">{{ $sale->Barcode }}</td>
                                        <td style="width: 17% !important; text-align: center;">{{ $sale->sukicard_number }}</td>
                                        <td style="text-align: center;">{{ "$".$sale->total_purchase }}</td>
                                        <td style="text-align: center;">{{ "$".$sale->money }}</td>
                                        <td style="text-align: center;">{{ "$".$sale->change }}</td>
                                        <td style="text-align: center;">{{ $sale->tx_code }}</td>
                                        <td style="text-align: center;">{{ $sale->receipt_number }}</td>
                                        <!-- <td style="text-align: center; width: 15% !important;">
                                            <a href="{{url('/')}}/admin/sales/edit/{{ $sale->id }}" class="btn btn-outline btn-circle dark btn-sm black">Edit</a>|
                                            <a href="{{url('/')}}/admin/sales/delete/{{ $sale->id }}" class="btn btn-outline btn-circle red btn-sm blue">Delete</a>
                                        </td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	