@extends('auditor.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- <h3 class="page-title">
            reportss
        </h3> -->
        <!-- <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}/auditor">auditor</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/')}}/auditor/reportss/add">Add Test reportss</a>
                    <i class="fa fa-angle-right"></i>
                </li>
            </ul>
        </div> -->
        @if(session()->has('message'))
            <div class="alert alert-{{ session()->get('messageTrigger') }}">
                {{ session()->get('message') }}
            </div>
        @endif
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-location-arrow" style="font-size: 18px !important; margin-top: 7px !important;"></i>
                            <span>Reports</span>&nbsp;
                            <!-- <a href="{{url('/')}}/auditor/reportss/add" class="btn btn-outline btn-circle btn-sm yellow">Add New reportss</a> -->
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-advance table-hover" id="customtables">
                        <!-- <table id="products-tab"> -->
                            <thead>
                                <tr>
                                    <td style="text-align: center;">User ID</td>
                                    <td style="text-align: center;">User Level</td>
                                    <td style="text-align: center; width: 17% !important;">Filename</td>
                                    <td style="text-align: center;">Date</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($reports as $reports)
                                    <tr>
                                        <td style="text-align: center;">{{ $reports->userid }}</td>
                                        <td style="text-align: center;">{{ ucwords($reports->user_level) }}</td>
                                        <td style="text-align: center; width: 30% !important;">{{ $reports->filename }}</td>
                                        <td style="text-align: center;">{{ $reports->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	