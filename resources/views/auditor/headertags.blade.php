<!DOCTYPE html>
<html lang="en">
    
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Auditor | Dashboard</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css">
<link href="{{ url('/') }}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
<link href="{{ url('/') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<!-- <link href="{{ url('/') }}/assets/global/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/> -->
<link href="{{ url('/') }}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<!-- <link href="{{ url('/') }}/{{ url('/') }}/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/> -->
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{ url('/') }}/assets/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/css/components.min.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/layouts/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/layouts/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{ url('/') }}/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('/') }}/css/adminstyle.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

<!-- JAVASCRIPTS -->
<script src="{{ url('/') }}/assets/global/plugins/jquery.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery-1.9.1.min.js"></script>
<script src="{{ url('/') }}/assets/global/scripts/app.js"></script>
<script src="{{ url('/') }}/assets/global/scripts/app.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- <script src="{{ url('/') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js"></script> -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="{{ url('/') }}/assets/global/scripts/datatable.js"></script>
<script src="{{ url('/') }}/assets/global/scripts/datatable.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/datatables/datatables.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
<script src="{{ url('/') }}/assets/pages/scripts/table-datatables-managed.min.js"></script>
<script src="{{ url('/') }}/assets/pages/scripts/table-datatables-editable.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
        $("#forModal").click(function(){
            $("#myModal").modal();
        });

        $('#customtables').DataTable({
	        dom: 'Bfrtip',
	        buttons: [
				{
				extend: 'excelHtml5',
				text: '<i class="fa fa-file-excel-o"></i> Excel',
				titleAttr: 'Export to Excel',
				title: 'EXCEL - '+'{{Route::currentRouteName()}}-{{ date("Y-m-d H:i:s") }}',
				exportOptions: {
				columns: ':not(:last-child)',
				}
				},
				{
				extend: 'csvHtml5',
				text: '<i class="fa fa-file-text-o"></i> CSV',
				titleAttr: 'CSV',
				title: 'csv - '+'{{Route::currentRouteName()}}-{{ date("Y-m-d H:i:s") }}',
				exportOptions: {
				columns: ':not(:last-child)',
				}
				},
				{
				extend: 'pdfHtml5',
				text: '<i class="fa fa-file-pdf-o"></i> PDF',
				titleAttr: 'PDF',
				title: 'PDF - '+'{{Route::currentRouteName()}}-{{ date("Y-m-d H:i:s") }}',
				exportOptions: {
				columns: ':not(:last-child)',
				},
				},
				{
				extend: 'print',
				exportOptions: {
				columns: ':visible'
				},
				customize: function(win) {
				$(win.document.body).find( 'table' ).find('td:last-child, th:last-child').remove();
				}
				},
			]
        });
    });
</script>
<!-- END OF JAVASCRIPTS -->

<!-- CHARTS -->
<script src="{{ url('/') }}/assets/global/plugins/counterup/jquery.waypoints.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/counterup/jquery.counterup.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/amcharts/amcharts/amcharts.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/flot/jquery.flot.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/flot/jquery.flot.categories.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/morris/morris.min.js"></script>
<script src="{{ url('/') }}/assets/pages/scripts/dashboard.min.js"></script>
<!-- END OF CHARTS -->
</head>
<!-- END HEAD -->
<body>
