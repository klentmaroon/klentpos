@extends('Auditor.master')

@section('title', 'Dashboard')

@section('contents')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Users
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}/auditor">Auditor</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{url('/')}}/auditor/cashiers">Cashiers</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a>Edit</a>
                    <i class="fa fa-angle-right"></i>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        @if(session()->has('message'))
            <div class="alert alert-{{ session()->get('messageTrigger') }}">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="adduser-wrapper">
                <form action="{{ route('auditor.update-user-submit') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="theid" value="{{$user->id}}">
                    <div class="form-group">
                        <label>Email Address</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input type="email" class="form-control" placeholder="Email Address" required name="useremail" value="{{$user->email}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>
                            <input type="password" class="form-control" placeholder="Password" name="userpassword">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Name</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i>N</i>
                            </span>
                            <input type="text" class="form-control" placeholder="Name" required name="username" value="{{$user->name}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>User Type</label>
                        <select class="form-control" name="usertype">
                            <option value="auditor" <?php if($user->user_level == 'auditor') {echo 'selected="selected" ';} ?> >Auditor</option>
                            <option value="cashier" <?php if($user->user_level == 'cashier') {echo 'selected="selected" ';} ?> >Cashier</option>
                            <option value="user" <?php if($user->user_level == 'user') {echo 'selected="selected" ';} ?> >User</option>
                        </select>
                    </div>

                    <div class="form-actions" style="padding-bottom: 0px !important;">
                        <div class="btn-set pull-left">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

@endsection	