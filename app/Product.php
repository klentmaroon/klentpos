<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $primaryKey = 'id';

    public function quantity() {
        return $this->hasMany(ProductQuantity::class);
    }
}
