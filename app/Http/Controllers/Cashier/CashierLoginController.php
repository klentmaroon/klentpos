<?php

namespace App\Http\Controllers\Cashier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CashierLoginController extends Controller
{

	public function __construct()
    {
      $this->middleware('guest:cashier');
    }

    public function showLoginForm(){
    	return view("cashier.cashier-login");
    }

    public function loginCashier(Request $request){
    	// Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      // Attempt to log the user in
      if (Auth::guard('cashier')->attempt(['email' => $request->email, 'password' => $request->password, 'user_level' => 'cashier'], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('cashier.dashboard'));
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
