<?php
namespace App\Http\Controllers\Cashier;

use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
use App\Product;
use Image;
use File;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:cashier');
    }

    public function index(Request $request)
    {
        return view('cashier.pos');
    }
}