<?php

namespace App\Http\Controllers\Cashier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class CashierHomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:cashier');
    }

    public function mainDashboard(){
    	return view("cashier.cashier");
    }

    public function Cashierlogout(Request $request) {
      //Auth::guard('admin')->logout();
      //dd( $request->user()->id );
      $user = Auth::user();
      $userToLogout = User::find($request->user()->id);
      Auth::setUser($userToLogout);
      Auth::logout();
      Auth::setUser($user);

      return redirect( route('cashier.login') );  
    }
}
