<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reports;
use Auth;

class DownloadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function download(Request $request) {
        $reports = new Reports;
        $reports->filename = $request->input('filename');
        $reports->user_level = $request->input('userlevel');
        $reports->userid = $request->input('userid');
        $currentroute = $request->input('currentroute');
        //dd($currentroute);
        $reports->save();

        return redirect()->back();
    }
}
