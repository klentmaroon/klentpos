<?php

namespace App\Http\Controllers\Admin;

use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
use App\Sales;
use App\Products;
use App\User;
use App\ProductQuantity;

class AdminSales extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = $this->getAllSales();
        //dd($sales);
        return view('admin.sales')->with('sales', $sales);
    }

    public function getAllSales()
    {
        /*$sale= DB::table('sales')->get();*/
        //$sale = new Sales;
        $sale = Sales::all()->sortByDesc('updated_at');
        return $sale;
    }

    public function showAddnew()
    {   
        $products = ProductQuantity::join('products', 'products.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.id as id')
            ->get();

        $users = User::where('user_level' , 'cashier')->get();
        //$products = Products::all();
        return view('Admin.salesform')->with('products', $products)->with('users', $users);
    }

    public function AddNewSales(Request $request)
    {
        $sales = new Sales;
        $sales->user_id = $request->input('userid');
        $productchoice = str_replace("p-", "", $request->input('productchoice'));
        $sales->productid = $productchoice;
        $sales->barcode = $request->input('barcode');
        $sales->tx_code = $request->input('txcode');
        $sales->receipt_number = $request->input('receiptno');
        $sales->sukicard_number = $request->input('sukicardno');
        $sales->total_purchase = $request->input('totalpur');
        $sales->money = $request->input('money');
        $sales->change = $request->input('change');
        $sales->quantity_purchased = $request->input('quantity');

        $sales->save();

        //return redirect()->route('admin-sales');

        if($sales->save()) {
            $messageTrigger = 'success';
            $message = 'Record Added Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function EditView(Request $request) {
        $salesid = $request->id;
        $sale = Sales::whereRaw("id = $salesid")->get();

        return view('admin.editsales', ['sale' => $sale]); 
    }

    public function updateSales(Request $request) {
        $sale = $request->input('salesid');
        $salesupdates = Sales::find($sale);
        
        
        $salesupdates->user_id = $request->input('userid');
        $salesupdates->total_purchase = $request->input('totalpur');
        $salesupdates->money = $request->input('money');
        $salesupdates->change = $request->input('change');

        $salesupdates->save();
        
        //return redirect()->route('admin-sales');

        if($salesupdates->save()) {
            $messageTrigger = 'success';
            $message = 'Record Updated Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function deleteSales($id) {
        Sales::where('id','=',$id)->delete();

        //return redirect()->route('admin-sales');

        if(!Sales::where('id','=',$id)->delete()) {
            $messageTrigger = 'success';
            $message = 'Record Deleted Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }
}
