<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Fileupload;
use DB;
use App\Product;

class AdminCsvUploader extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.csvuploader');
    }
    public function uploadCSV(Request $request)
    {
        //get file
        $upload = $request->file('csvfile');
        $filepath = $upload->getRealPath();

        $csvData = file_get_contents($upload);
        $lines = explode(PHP_EOL, $csvData);

        $array = array();
        foreach ($lines as $line) {
            $array[] = str_getcsv($line);
        }

        $DBtable = new Fileupload;

        foreach ($array as $key => $value) {
            if (empty($value[0])) {
                break;
            }
                $name = $value[0];
                $description = $value[3];
                $sku = $value[2];
                $image = $value[4];
                $asin = $value[1];
            
            if(empty($name)){ $name = "xxx"; }
            if(empty($description)){ $description = "xxx"; }
            if(empty($sku)){ $sku = "xxx"; }
            if(empty($image)){ $image = "xxx"; }
            if(empty($asin)){ $asin = "xxx"; }
            
            $csvData = $arrayName = array('name' => $name, 'description' => $description, 'image' => $image, 'asin' => $asin, 'sku' => $sku );

            Product::insert($csvData);
        }
        $response = "Upload Success !";
        return $response;
    }
}
