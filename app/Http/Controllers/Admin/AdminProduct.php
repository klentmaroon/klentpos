<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductQuantity;
use App\Product;
use App\Sales;
use App\Reports;
use Auth;
use DB;
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
use File;
use PDF;

class AdminProduct extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // $product = $this->getAllproduct();
        // $product = Product::all();
        $products = Product::with('quantity')->orderBy('created_at','desc')->get();
        
        return view('admin.product')->with('products', $products);
        // return $products;
    }

    /*public function getAllproduct() {
        
        $product = DB::table('product_quantity')
            ->join('product', 'product.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.id as id')
            ->orderBy('product_quantity.updated_at', 'desc')
            ->get();

        return $product;
    }*/

    public function viewProduct(Request $request) {
        /*$prodid = $request->id;
        $viewprod = DB::table('product_quantity')
            ->join('product', 'product.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.product_id as id')
            ->where('product_quantity.product_id', '=', $prodid)
            ->get();
        return view('Admin.productdetails', ['viewprod' => $viewprod]); */ 
        //$product = Product::where('id',$productid)->first();
        //$productquant = DB::table('product_quantity')->where('product_id','=',$productid)->get(); 

        $productid = $request->id;
        $product = Product::with('quantity')->find($productid);

        return view('Admin.productdetails')->with('product', $product); 
    }

    public function showAddnew()
    {
        return view('Admin.addproduct');
    }

    public function AddNewproduct(Request $request) {
        $product = new product;
        
        $product->name = $request->input('productname');
        $product->description = $request->input('productdesc');
        $product->sku = $request->input('productsku');
        $product->asin = $request->input('productasin');
        $product->barcode = $request->input('productbarcode');

        /*$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 8;
        $randomstring = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);*/

        if( $request->hasFile('productimage') )
        {
            $file = $request->file('productimage');
            $destinationPath = public_path('upload');

            if(!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
            }

            $mimeType = ['image/jpeg', 'application/pdf'];
            $fileType = $file->getMimeType();

            $extension = $file->getClientOriginalExtension();
            $md5 = $file->getClientOriginalName();
            $filename = $md5;
            $file->move($destinationPath, $filename);

            $input['productimage'] = url('upload/' .$filename);
            $filepathname = 'upload/' .$filename;

            $product->image = $filepathname;
        }
        $product->save();

        /*$singleProduct = product::where('UNCode','=',$randomstring)->get()->first();
        $codeID = $singleProduct->id;*/
        $product_quantity = new ProductQuantity;
        $product_quantity->product_id = $product->id; // Id of the brand
        $product_quantity->quantity = $request->input('productquantity');
        $product_quantity->price = $request->input('productprice');
        $product_quantity->original_price = $request->input('productorigprice');
        $product_quantity->sale_price = $request->input('productsaleprice');
        $product_quantity->wholesale_price = $request->input('productwholesaleprice');
        $product_quantity->save();
        
        if($product->save() && $product_quantity->save()) {
            $messageTrigger = 'success';
            $message = 'Record Added Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
        //return redirect()->route('admin-product');
    }

    public function EditView(Request $request) {
        $productid = $request->id;
        $product = Product::where('id','=',$productid)->get();
        $productquant = ProductQuantity::where('product_id','=',$productid)->get();
        
        $product = $product;
        $product_quantity = $productquant;
        return view('Admin.editproduct', ['product' => $product, 'product_quantity' => $product_quantity]);

        /*$product = Product::with('quantity')->find($productid);
        return $product;*/
        //return view('Admin.editproduct')->with('product', $product);
    }

    public function updateProduct(Request $request) {
        $prodid = $request->input('productid');
        $productid = $request->input('prodid');
        $product = Product::find($prodid);

        $product->name = $request->input('productname');
        $product->description = $request->input('productdesc');
        $product->sku = $request->input('productsku');
        $product->asin = $request->input('productasin');
        $product->barcode = $request->input('productbarcode');

        /*$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 8;
        $randomstring = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);*/

        //$product->UNCode = $randomstring;

        if( $request->hasFile('productimage') )
        {
            $file = $request->file('productimage');
            $destinationPath = public_path('upload');

            if(!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
            }

            $mimeType = ['image/jpeg', 'application/pdf'];
            $fileType = $file->getMimeType();

            $extension = $file->getClientOriginalExtension();
            $md5 = $file->getClientOriginalName();
            $filename = $md5;
            $file->move($destinationPath, $filename);

            $input['productimage'] = url('upload/' .$filename);
            $filepathname = 'upload/' .$filename;

            $product->image = $filepathname;
        }
        
        //return redirect('admin/product/edit/'.$prodid);
        if($product->save()) {
            $messageTrigger = 'success';
            $message = 'Record Updated Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function deleteProduct($id) {
        //DB::table('product_quantity')->where('id','=',$id)->delete();
        Product::with('quantity')->where('id',$id)->delete();
        //return redirect()->route('admin-product');

        if(!Product::with('quantity')->where('id',$id)->delete()) {
            $messageTrigger = 'success';
            $message = 'Record Deleted Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function editQuantity(Request $request) {
        $productid = $request->id;
        $productquant = ProductQuantity::where('id','=',$productid)->get();
        $product_quantity = $productquant;

        return view('Admin.editproductquantitytable', ['product_quantity' => $product_quantity]);
    }

    public function saveQuantity(Request $request) {
        $prodid = $request->input('prodid');
        //dd($prodid);
        $product_quantity = new ProductQuantity;

        $product_quantity->product_id = $prodid;
        $product_quantity->quantity = $request->input('productquantity');
        $product_quantity->price = $request->input('productprice');
        $product_quantity->original_price = $request->input('productorigprice');
        $product_quantity->sale_price = $request->input('productsaleprice');
        $product_quantity->wholesale_price = $request->input('productwholesaleprice');

        if($product_quantity->save()) {
            $messageTrigger = 'success';
            $message = 'Record Added Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function updateQuantity(Request $request) {
        $productquantityid = $request->input('productquantityid');
        $product_id = $request->input('product_id');
        $productquantity = ProductQuantity::find($productquantityid);

        $productquantity->quantity = $request->input('productquantity');
        $productquantity->price = $request->input('productqprice');
        $productquantity->original_price = $request->input('productqorigprice');
        $productquantity->sale_price = $request->input('productqsaleprice');
        $productquantity->wholesale_price = $request->input('productqwholesaleprice'); 

        $productquantity->save();
        //return redirect('admin/product/edit/'.$product_id);

        if($productquantity->save()) {
            $messageTrigger = 'success';
            $message = 'Record Updated Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect('admin/product/edit/'.$product_id)->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function deleteproductquantity($id) {
        ProductQuantity::where('id','=',$id)->delete();

        //return redirect()->route('admin-product');

        if(!ProductQuantity::where('id','=',$id)->delete()) {
            $messageTrigger = 'success';
            $message = 'Record Deleted Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function topsaleable() {
        $result = $this->getSaleAble();
        //dd($result);
        return view("admin.topsaleable")->with("products" , $result);
    }

    public function getSaleAble() {
        $product = ProductQuantity::join('products', 'products.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.id as id')
            ->get();

            //dd($product);
        $xsamp = 0;
        foreach ($product as $key) {
            $countsales = Sales::where(['productid' => $key->id])->get()->count();

            if ($countsales != 0) {
                $xsamp++;
                $spec_sales = Sales::where(['productid' => $key->id])->get();
                $i = 0;
                foreach ($spec_sales as $specs) {
                    $i = $i + $specs->quantity_purchased;
                    $result[$key->id] = array(
                        'count' => $i,
                        'name' => $key->name,
                        'image' => $key->image,
                        'price' => $key->price,
                        'product_id' => $key->product_id,
                        'barcode' => $key->barcode,
                    );
                }
            }
        }
        if ($xsamp != 0) {
            arsort($result);
        }else{ $result = false; }
        return $result;
    }

    public function NonMovingproduct() {
        $result = $this->GetNonMoving();
        //dd($result);
        return view("admin.nonmovingproducts")->with("products" , $result);
    }

    public function GetNonMoving() {
        $product = ProductQuantity::join('products', 'products.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.id as id')
            ->get();

        $xsamp = 0;
        foreach ($product as $key) {
            $countsales = Sales::where(['productid' => $key->id])->get();
            $i = 0;

        if ($countsales) {
            $xsamp++;
            foreach ($countsales as $nm) {
                $i = $i + $nm->quantity_purchased;
            }
                $tot = $key->quantity * .20;

                if ($tot >= $i) {
                    $result[$key->id] = array(
                        'count' => $i,
                        'name' => $key->name,
                        'image' => $key->image,
                        'price' => $key->price,
                        'product_id' => $key->product_id,
                        'barcode' => $key->barcode,
                    );
                }
            
            }
        }

        if ($xsamp != 0) {
            arsort($result);
        }else{ $result = false; }
        
        return $result;
    }

    public function download(Request $request) {
        $reports = new Reports;
        $reports->filename = $request->input('filename');
        $reports->user_level = $request->input('userlevel');
        $reports->userid = $request->input('userid');
        $reports->save();

        return redirect( route('admin-product') );
    }

}