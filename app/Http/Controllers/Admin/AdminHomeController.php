<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Sales;
use Auth;
use App\User;
use App\Reports;

class AdminHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }
    public function mainDashboard()
    {
        $sales = Sales::get();

        return view('admin.dashboard', ['sales' => $sales]);
    }
    public function mainProducts()
    {
        return view('admin.products');
    }
    public function mainOrders()
    {
        return view('admin.orders');
    }
    public function mainSales()
    {
        return view('admin.sales');
    }

    public function mainUserlist()
    {
        $users = $this->getUsers();
        return view('admin.userlist')->with("users" , $users);
    }
    public function getUsers()
    {
        $users = User::where('user_level' ,'!=', 'admin')->orderBy('updated_at', 'desc')->get();
        return $users;
    }

    public function mainSettings()
    {
        return view('admin.settings');
    }
    public function maintestform()
    {
        return view('admin.testform');
    }
    public function Adminlogout(Request $request) {
      //Auth::guard('admin')->logout();
      //dd( $request->user()->id );
      $user = Auth::user();
      $userToLogout = User::find($request->user()->id);
      Auth::setUser($userToLogout);
      Auth::logout();
      Auth::setUser($user);

      return redirect( route('admin.login') );  
    }

    public function getReports(){
      $reports = Reports::all();

      return view('admin.reports')->with("reports" , $reports);
    }
}
