<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Sales;
use DB;

class AdminUsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function showAddUserForm(){
    	return view("admin.useradd");
    }

    public function addUser(Request $request){
    	$user = new User;
    	$user->name = $request->input('username');
    	$user->email = $request->input('useremail');
    	$user->password = Hash::make( $request->input('userpassword') );
    	$user->user_level = $request->input('usertype');
    	$user->save();

    	//return redirect( route('admin-userlist') );

        if($user->save()) {
            $messageTrigger = 'success';
            $message = 'Record Added Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function deleteuser($id){
        //User::find($id)->delete();
        //return redirect(route('admin-userlist'));

        if(User::find($id)->delete()) {
            $messageTrigger = 'success';
            $message = 'Record Deleted Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function edituser($id){
        $user = User::find($id);
        return view('auditor.useredit')->with('user',$user);
    }

    public function update(Request $request){
        $user = User::find($request->input("theid"));

        $user->name = $request->input("username");
        $user->email = $request->input("useremail");
        $user->user_level = $request->input("usertype");

        if( !empty( $request->input("password") ) ){
            $user->password = Hash::make($request->input("userpassword"));
        }
        $user->save();

        //return redirect(route('admin-userlist'));

        if($user->save()) {
            $messageTrigger = 'success';
            $message = 'Record Updated Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function CashierTopPerformers(){
        $user = $this->GetTopPerformers();
        //dd($user);
        return view('admin.topperformers')->with('users',$user);
    }

    public function GetTopPerformers(){
        //$user = User::where('user_level' , 'cashier')->get();
        /*$user = DB::table('sales')
            ->join('users', 'users.id', '=', 'sales.user_id')
            ->select('*','sales.id as id')
            ->get();*/

        $user = user::all();

        foreach ($user as $key) {
            $countuser = Sales::where(['user_id' => $key->id])->get()->count();

            if ($countuser != 0) {

                $result[$key->id] = array( 
                    'count' => $countuser,
                    'name' => $key->name,
                    'email' => $key->email,
                );;
            }
        }
        return $result;
    }
}
