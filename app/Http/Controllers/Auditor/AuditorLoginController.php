<?php

namespace App\Http\Controllers\Auditor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AuditorLoginController extends Controller
{

	public function __construct()
    {
      $this->middleware('guest:auditor');
    }

    public function showLoginForm(){
    	return view("auditor.auditor-login");
    }

    public function loginAuditor(Request $request){
    	// Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      // Attempt to log the user in
      if (Auth::guard('auditor')->attempt(['email' => $request->email, 'password' => $request->password, 'user_level' => 'auditor'], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('auditor.dashboard'));
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}