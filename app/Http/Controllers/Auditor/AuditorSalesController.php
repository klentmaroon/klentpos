<?php

namespace App\Http\Controllers\Auditor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sales;

class AuditorSalesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:auditor');
    }
    
    public function index(){
		$sales = $this->getAllSales();
		return view('auditor.sales')->with('sales' , $sales);
	}

	public function getAllSales(){
		$sales = Sales::all();
		return $sales;
	}
}
