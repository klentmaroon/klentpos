<?php

namespace App\Http\Controllers\Auditor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sales;
use App\User;
use App\Reports;
use Auth;

class AuditorHomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:auditor');
    }

    public function AuditorDashboard(){
    	$sales = $this->getSales();

    	return view("auditor.dashboard")->with('sales', $sales);
    }

    public function getSales(){
    	$sales = Sales::all();
    	return $sales;
    }

    public function Auditorlogout(Request $request) {
      $user = Auth::user();
      $userToLogout = User::find($request->user()->id);
      Auth::setUser($userToLogout);
      Auth::logout();
      Auth::setUser($user);

      return redirect( route('auditor.login') );  
    }

    public function getReports(){
      $reports = Reports::all();

      return view('auditor.reports')->with("reports" , $reports);
    }
}
