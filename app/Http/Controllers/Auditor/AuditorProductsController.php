<?php

namespace App\Http\Controllers\Auditor;

use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
use App\Products;
use App\ProductQuantity;
use File;
use App\Sales;
use App\Product;

class AuditorProductsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:auditor');
    }
    
	public function index(){
		//$products = $this->getAllProducts();
        $products = Product::with('quantity')->orderBy('created_at','desc')->get();
		return view('auditor.products')->with('products' , $products);
	}

	/*public function getAllProducts(){
		$products = ProductQuantity::join('products', 'products.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.id as id')
            ->orderBy('product_quantity.updated_at', 'desc')
            ->get();

		return $products;
	}*/
	public function topsaleable() {
        $result = $this->getSaleAble();
        //dd($result);
        return view("auditor.topsaleable")->with("products" , $result);
    }

    public function getSaleAble() {
        $product = ProductQuantity::join('products', 'products.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.id as id')
            ->get();

            //dd($product);
        $xsamp = 0;
        foreach ($product as $key) {
            $countsales = Sales::where(['productid' => $key->id])->get()->count();

            if ($countsales != 0) {
                $xsamp++;
                $spec_sales = Sales::where(['productid' => $key->id])->get();
                $i = 0;
                foreach ($spec_sales as $specs) {
                    $i = $i + $specs->quantity_purchased;
                    $result[$key->id] = array(
                        'count' => $i,
                        'name' => $key->name,
                        'image' => $key->image,
                        'price' => $key->price,
                        'product_id' => $key->product_id,
                        'barcode' => $key->barcode,
                    );
                }
            }
        }
        if ($xsamp != 0) {
            arsort($result);
        }else{ $result = false; }
        return $result;
    }

    public function NonMovingProducts() {
        $result = $this->GetNonMoving();
        //dd($result);
        return view("auditor.nonmovingproducts")->with("products" , $result);
    }

    public function GetNonMoving() {
        $product = ProductQuantity::join('products', 'products.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.id as id')
            ->get();

        $xsamp = 0;
        foreach ($product as $key) {
            $countsales = Sales::where(['productid' => $key->id])->get();
            $i = 0;

        if ($countsales) {
            $xsamp++;
            foreach ($countsales as $nm) {
                $i = $i + $nm->quantity_purchased;
            }
                $tot = $key->quantity * .20;

                if ($tot >= $i) {
                    $result[$key->id] = array(
                        'count' => $i,
                        'name' => $key->name,
                        'image' => $key->image,
                        'price' => $key->price,
                        'product_id' => $key->product_id,
                        'barcode' => $key->barcode,
                    );
                }
            
            }
        }

        if ($xsamp != 0) {
            arsort($result);
        }else{ $result = false; }
        
        return $result;
    }

    public function viewProduct(Request $request) {
        /*$prodid = $request->id;
        $viewprod = DB::table('product_quantity')
            ->join('products', 'products.id', '=', 'product_quantity.product_id')
            ->select('*','product_quantity.product_id as id')
            ->where('product_quantity.product_id', '=', $prodid)
            ->get();

        return view('auditor.productdetails', ['viewprod' => $viewprod]); */ 

        $productid = $request->id;
        $products = Product::where('id','=',$productid)->get();
        $productquant = ProductQuantity::where('product_id','=',$productid)->get(); 

        $product = $products;
        $product_quantity = $productquant;

        return view('auditor.productdetails', ['product' => $product, 'product_quantity' => $product_quantity]); 
    }

    public function showAddnew()
    {
        return view('auditor.addproduct');
    }

    public function AddNewProduct(Request $request) {
        $products = new Product;
        
        $products->name = $request->input('productname');
        $products->description = $request->input('productdesc');
        $products->sku = $request->input('productsku');
        $products->asin = $request->input('productasin');
        $products->barcode = $request->input('productbarcode');

        /*$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 8;
        $randomstring = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);

        $products->UNCode = $randomstring;*/

        if( $request->hasFile('productimage') )
        {
            $file = $request->file('productimage');
            $destinationPath = public_path('upload');

            if(!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
            }

            $mimeType = ['image/jpeg', 'application/pdf'];
            $fileType = $file->getMimeType();

            $extension = $file->getClientOriginalExtension();
            $md5 = $file->getClientOriginalName();
            $filename = $md5;
            $file->move($destinationPath, $filename);

            $input['productimage'] = url('upload/' .$filename);
            $filepathname = 'upload/' .$filename;

            $products->image = $filepathname;
        }
        $products->save();

        /*$singleProduct = Products::where('UNCode','=',$randomstring)->get()->first();
        $codeID = $singleProduct->id;*/
        $product_quantity = new ProductQuantity;
        $product_quantity->product_id = $products->id; // Id of the brand
        $product_quantity->quantity = $request->input('productquantity');
        $product_quantity->price = $request->input('productprice');
        $product_quantity->original_price = $request->input('productorigprice');
        $product_quantity->sale_price = $request->input('productsaleprice');
        $product_quantity->wholesale_price = $request->input('productwholesaleprice');
        $product_quantity->save();

        if($products->save() && $product_quantity->save()) {
            $messageTrigger = 'success';
            $message = 'Record Added Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
        /*return redirect()->route('auditor.products');*/
    }

    public function EditView(Request $request) {
        $productid = $request->id;
        $products = Product::where('id','=',$productid)->get();
        $productquant = ProductQuantity::where('product_id','=',$productid)->get(); 

        $product = $products;
        $product_quantity = $productquant;

        return view('auditor.editproduct', ['product' => $product, 'product_quantity' => $product_quantity]);
    }

    public function updateProduct(Request $request) {
        $prodid = $request->input('productid');
        $productid = $request->input('prodid');
        $products = Products::find($prodid);

        $products->name = $request->input('productname');
        $products->description = $request->input('productdesc');
        $products->sku = $request->input('productsku');
        $products->asin = $request->input('productasin');
        $products->barcode = $request->input('productbarcode');

        /*$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 8;
        $randomstring = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);

        $products->UNCode = $randomstring;*/

        if( $request->hasFile('productimage') )
        {
            $file = $request->file('productimage');
            $destinationPath = public_path('upload');

            if(!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
            }

            $mimeType = ['image/jpeg', 'application/pdf'];
            $fileType = $file->getMimeType();

            $extension = $file->getClientOriginalExtension();
            $md5 = $file->getClientOriginalName();
            $filename = $md5;
            $file->move($destinationPath, $filename);

            $input['productimage'] = url('upload/' .$filename);
            $filepathname = 'upload/' .$filename;

            $products->image = $filepathname;
        }
        $products->save();
        
        //return redirect('auditor/products/edit/'.$prodid);
        if($product->save()) {
            $messageTrigger = 'success';
            $message = 'Record Updated Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function deleteProduct($id) {
        //Product::find($id)->delete();
        Product::with('quantity')->where('id',$id)->delete();

        if(!Product::with('quantity')->where('id',$id)->delete()) {
            $messageTrigger = 'success';
            $message = 'Record Deleted Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function editQuantity(Request $request) {
        $productid = $request->id;
        $productquant = ProductQuantity::where('id','=',$productid)->get();
        $product_quantity = $productquant;

        return view('auditor.editproductquantitytable', ['product_quantity' => $product_quantity]);
    }

    public function saveQuantity(Request $request) {
        $prodid = $request->input('prodid');
        $product_quantity = new ProductQuantity;

        $product_quantity->product_id = $prodid;
        $product_quantity->quantity = $request->input('productquantity');
        $product_quantity->price = $request->input('productprice');
        $product_quantity->original_price = $request->input('productorigprice');
        $product_quantity->sale_price = $request->input('productsaleprice');
        $product_quantity->wholesale_price = $request->input('productwholesaleprice');
        $product_quantity->save();

        /*return redirect('/auditor/products/edit/'.$prodid);*/

        if($product_quantity->save()) {
            $messageTrigger = 'success';
            $message = 'Record Added Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function updateQuantity(Request $request) {
        $productquantityid = $request->input('productquantityid');
        $product_id = $request->input('product_id');
        $productquantity = ProductQuantity::find($productquantityid);

        $productquantity->quantity = $request->input('productquantity');
        $productquantity->price = $request->input('productqprice');
        $productquantity->original_price = $request->input('productqorigprice');
        $productquantity->sale_price = $request->input('productqsaleprice');
        $productquantity->wholesale_price = $request->input('productqwholesaleprice'); 

        $productquantity->save();
        //return redirect('auditor/product/edit/'.$product_id);

        if($productquantity->save()) {
            $messageTrigger = 'success';
            $message = 'Record Updated Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect('auditor/product/edit/'.$product_id)->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

    public function deleteproductquantity($id) {
        ProductQuantity::where('id','=',$id)->delete();

        //return redirect()->route('auditor-products');

        if(!ProductQuantity::where('id','=',$id)->delete()) {
            $messageTrigger = 'success';
            $message = 'Record Deleted Successfully!';
        } else {
            $messageTrigger = 'danger';
            $message = 'Error occured!';
        }
        return redirect()->back()->with(['messageTrigger' => $messageTrigger,'message' => $message]);
    }

}
