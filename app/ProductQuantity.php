<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductQuantity extends Model
{
	protected $table = 'product_quantity';

    protected $primaryKey = 'id';

    public function product() {
        return $this->belongsTo(Product::class);
    }
}
