<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'admin@pos.com',
            'user_level' => 'admin',
            'password' => bcrypt('secret'),
            'updated_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'cashier@pos.com',
            'user_level' => 'cashier',
            'password' => bcrypt('secret'),
            'updated_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'auditor@pos.com',
            'user_level' => 'auditor',
            'password' => bcrypt('secret'),
            'updated_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'user@pos.com',
            'user_level' => 'user',
            'password' => bcrypt('secret'),
            'updated_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
