<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToSalesTab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('tx_code');
            $table->string('sukicard_number');
            $table->double('total_purchase', 8, 2);
            $table->double('money', 8, 2);
            $table->double('change', 8, 2);
            $table->integer('quantity_purchased');
            $table->integer('productid');
            $table->string('barcode');
            $table->string('receipt_number');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
