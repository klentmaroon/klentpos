<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('user.dashboard');
Route::post('/download', 'DownloadController@download')->name('download-pdf');

Route::prefix('admin')->group(function() {
    Route::get('/login', 'Admin\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\AdminLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Admin\AdminHomeController@Adminlogout')->name('admin.logout');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    
	Route::get('/dashboard', 'Admin\AdminHomeController@mainDashboard')->name('admin-dashboard');
    Route::get('/products', 'Admin\AdminProduct@index')->name('admin-product');
    Route::get('/orders', 'Admin\AdminHomeController@mainOrders')->name('admin-orders');
    Route::get('/sales', 'Admin\AdminSales@index')->name('admin-sales');
    Route::get('/users', 'Admin\AdminHomeController@mainUserlist')->name('admin-userlist');
    Route::get('/settings', 'Admin\AdminHomeController@mainSettings')->name('admin-settings');
    
    Route::get('/uploadcsv', 'Admin\AdminCsvUploader@index')->name('admin-uploadcsv');
    Route::post('/uploadcsv-submit', 'Admin\AdminCsvUploader@uploadCSV')->name('admin-uploadcsvfile');
    
    Route::get('/product/add', 'Admin\AdminProduct@showAddnew')->name('admin-new-product');
    Route::post('/product/add/submit', 'Admin\AdminProduct@AddNewproduct')->name('admin-new-product-submit');
    Route::get('/product/details/{id}', 'Admin\AdminProduct@viewProduct')->name('admin-view-product');
    Route::get('/product/edit/{id}', 'Admin\AdminProduct@EditView')->name('admin-edit-product');
    
    Route::post('/product/update', 'Admin\AdminProduct@updateProduct')->name('admin-update-product');
    Route::get('/product/delete/{id}', 'Admin\AdminProduct@deleteProduct')->name('admin-delete-product');
    Route::get('/products/top-saleable', 'Admin\AdminProduct@topsaleable')->name('admin-topsaleable-product');
    Route::get('/products/non-moving', 'Admin\AdminProduct@NonMovingproduct')->name('admin-nonmoving-product');

    /*Product Quantity Table*/
    Route::get('/product/quantity/edit/{id}', 'Admin\AdminProduct@editQuantity')->name('admin-edit-quantity');
    Route::post('/product/quantity/save', 'Admin\AdminProduct@saveQuantity')->name('admin-save-quantity');
    Route::post('/product/quantity/update', 'Admin\AdminProduct@updateQuantity')->name('admin-update-quantity');
    Route::get('/product/quantity/delete/{id}', 'Admin\AdminProduct@deleteproductquantity')->name('admin-delete-product-quantity');
    /*End of Product Quantity Table*/

    Route::get('/sales/add', 'Admin\AdminSales@showAddnew')->name('admin-add-sales');
    Route::post('/sales/add/submit', 'Admin\AdminSales@AddNewSales')->name('admin-sales-addform-submit');
    Route::get('/sales/edit/{id}', 'Admin\AdminSales@EditView')->name('admin-edit-sales');
    Route::post('/sales/update', 'Admin\AdminSales@updateSales')->name('admin-update-sales');
    Route::get('/sales/delete/{id}', 'Admin\AdminSales@deleteSales')->name('admin-delete-sales');

    /*Cashier Users*/
    Route::get('/user/add', 'Admin\AdminUsersController@showAddUserForm')->name('admin.add-user');
    Route::post('/user/add', 'Admin\AdminUsersController@addUser')->name('admin.add-user-submit');
    Route::get('/user/delete/{id}', 'Admin\AdminUsersController@deleteuser')->name('admin.delete-user');
    Route::get('/user/edit/{id}', 'Admin\AdminUsersController@edituser')->name('admin.edit-user');
    Route::post('/user/update', 'Admin\AdminUsersController@update')->name('admin.update-user-submit');
    Route::get('/user/cashier-top-performers', 'Admin\AdminUsersController@CashierTopPerformers')->name('admin.top-performers-user');
    /*Cashier Users*/

    /*Reports*/
    Route::get('/reports', 'Admin\AdminHomeController@getReports')->name('admin.reports');
    /*Reports*/
});

Route::prefix('cashier')->group(function() {
    Route::get('/login', 'Cashier\CashierLoginController@showLoginForm')->name('cashier.login');
    Route::post('/login', 'Cashier\CashierLoginController@loginCashier')->name('cashier.login.submit');
    Route::post('/logout', 'Cashier\CashierHomeController@Cashierlogout')->name('cashier.logout');
    Route::get('/', 'CashierController@index');

    Route::get('/dashboard', 'Cashier\MainController@index')->name('cashier.dashboard');
});

Route::prefix('auditor')->group(function() {

    Route::get('/login', 'Auditor\AuditorLoginController@showLoginForm')->name('auditor.login');
    Route::post('/login', 'Auditor\AuditorLoginController@loginAuditor')->name('auditor.login.submit');
    Route::get('/', 'AuditorController@index')->name('auditor');
    Route::post('/logout', 'Auditor\AuditorHomeController@Auditorlogout')->name('auditor.logout');

    Route::get('/dashboard', 'Auditor\AuditorHomeController@AuditorDashboard')->name('auditor.dashboard');
    Route::get('/products', 'Auditor\AuditorproductsController@index')->name('auditor.products');
    Route::get('/sales', 'Auditor\AuditorSalesController@index')->name('auditor.sales');
    Route::get('/reports', 'Auditor\AuditorHomeController@getReports')->name('auditor.reports');

    Route::get('/products/top-saleable', 'Auditor\AuditorproductsController@topsaleable')->name('auditor-topsaleable-product');
    Route::get('/products/non-moving', 'Auditor\AuditorproductsController@NonMovingProducts')->name('auditor-nonmoving-product');

    Route::get('/cashiers', 'Auditor\AuditorUserController@AllCashiers')->name('auditor.cashiers');
    Route::get('/cashier/add', 'Auditor\AuditorUserController@showAddUserForm')->name('auditor.add-user');
    Route::post('/cashier/add', 'Auditor\AuditorUserController@addUser')->name('auditor.add-user-submit');
    Route::get('/cashier/cashier-top-performers', 'Auditor\AuditorUserController@CashierTopPerformers')->name('auditor.top-performers-user');
    Route::get('/cashier/delete/{id}', 'Auditor\AuditorUserController@deleteuser')->name('auditor.delete-user');
    Route::get('/cashier/edit/{id}', 'Auditor\AuditorUserController@edituser')->name('auditor.edit-user');
    Route::post('/cashier/update', 'Auditor\AuditorUserController@update')->name('auditor.update-user-submit');

    Route::get('/product/add', 'auditor\AuditorproductsController@showAddnew')->name('auditor-new-product');
    Route::post('/product/add/submit', 'auditor\AuditorproductsController@AddNewproduct')->name('auditor-new-product-submit');
    Route::get('/product/details/{id}', 'auditor\AuditorproductsController@viewProduct')->name('auditor-view-product');
    Route::get('/product/edit/{id}', 'auditor\AuditorproductsController@EditView')->name('auditor-edit-product');
    
    Route::post('/product/update', 'auditor\AuditorproductsController@updateProduct')->name('auditor-update-product');
    Route::get('/product/delete/{id}', 'auditor\AuditorproductsController@deleteProduct')->name('auditor-delete-product');

    Route::get('/product/quantity/edit/{id}', 'auditor\AuditorproductsController@editQuantity')->name('auditor-edit-quantity');
    Route::post('/product/quantity/save', 'auditor\AuditorproductsController@saveQuantity')->name('auditor-save-quantity');
    Route::post('/product/quantity/update', 'auditor\AuditorproductsController@updateQuantity')->name('auditor-update-quantity');
    Route::get('/product/quantity/delete/{id}', 'auditor\AuditorproductsController@deleteproductquantity')->name('auditor-delete-product-quantity');

});